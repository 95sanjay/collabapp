package com.info.collab.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.info.collab.R;
import com.info.collab.fragements.BusinessInfoFragment;
import com.info.collab.fragements.ProductInfoFragment;
import com.info.collab.fragements.ServiceInfoFragment;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.Constant;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity  {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    ImageView backBtn,cover_image;
    CircleImageView logoImg;
    private int[] tabIcons = {R.drawable.ic_warning, R.drawable.ic_warning, R.drawable.ic_warning};

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        setContentView(R.layout.profile);
        init();
    }

    void  init(){
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        cover_image = (ImageView) findViewById(R.id.cover_image);
        logoImg = (CircleImageView) findViewById(R.id.logoImg);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Glide.with(cover_image.getContext()).load(AppPreferences.getStringPreference(getApplicationContext(),Constant.business_cover_image)).into(cover_image);
        Glide.with(logoImg.getContext()).load(AppPreferences.getStringPreference(getApplicationContext(),Constant.business_logo)).into(logoImg);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new BusinessInfoFragment(), "Business Info");
        adapter.addFragment(new ProductInfoFragment(), "Product Info");
        adapter.addFragment(new ServiceInfoFragment(), "Service Info");
        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
    }
}