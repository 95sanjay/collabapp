package com.info.collab.fragements;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.info.collab.R;
import com.info.collab.activity.CategoryActivity;
import com.info.collab.activity.ProductInfo;
import com.info.collab.adapter.ChatAdapter;
import com.info.collab.adapter.NewInAdapter;
import com.info.collab.adapter.ProductCategoryAdapter;
import com.info.collab.adapter.ProductCategoryAdapter2;
import com.info.collab.modalClass.Feed;
import com.info.collab.modalClass.Product;
import com.info.collab.modalClass.ProductCategory;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.Constant;
import com.info.collab.utils.OnItemCategoryClick;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class CategoryFragment extends BaseFragment {
    RecyclerView chat_rv;
    private View view;
    private ArrayList<Feed> chatArrayList = new ArrayList<>();
    private ChatAdapter feedAdapter;
    private ArrayList<Product> productslist;
    private SearchView edt_search;
    private NewInAdapter productAdapter;
    private ArrayList<ProductCategory> productcategourylist;

    public static CategoryFragment newInstance(Bundle args) {
        CategoryFragment fragment = new CategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.new_in_fragment, container, false);
        init();
        return view;
    }

    void init(){
        chat_rv = view.findViewById(R.id.chat_rv);
        edt_search = view.findViewById(R.id.edt_search);
        edt_search.setVisibility(View.GONE);
        ImageView back_btn = view.findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        TextView title = view.findViewById(R.id.title);
        title.setText("Product Category");
        productCategouryApi();
    }


    private void productCategouryApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(getContext(), Constant.uni_id ))
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(getContext(), Constant.token_id ));
        RequestBody requestBody = builder.build();
        RetrofitService.Category_list(new Dialog(getContext()), retrofitApiClient.Category_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("category_list");
                        JSONObject object = null;
                        productcategourylist = new ArrayList<>();
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            object = jsonArray.getJSONObject(i);
                            ProductCategory product = new ProductCategory();
                            product.setCategory_identifier(object.getString("category_identifier"));
                            product.setCategory_english_name(object.getString("category_english_name"));
                            product.setCategory_arabic_name(object.getString("category_arabic_name"));
                            product.setCategory_description(object.getString("category_description"));
                            product.setCategory_main_image(object.getString("category_img"));
                            productcategourylist.add(product);
                        }
                        ProductCategoryAdapter2 productCategoryAdapterAdapter = new ProductCategoryAdapter2(productcategourylist, new OnItemCategoryClick() {
                            @Override
                            public void onItemClick(ProductCategory item) {
                                Intent intent = new Intent(getContext(), CategoryActivity.class);
                                intent.putExtra("category_id",item.getCategory_identifier());
                                startActivity(intent);
                            }
                        });
                        chat_rv.setLayoutManager(new GridLayoutManager(getContext(), 2));
                        chat_rv.setItemAnimator(new DefaultItemAnimator());
                        chat_rv.setAdapter(productCategoryAdapterAdapter);
                        productCategoryAdapterAdapter.notifyDataSetChanged();
                    }else {
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseFailed(String error) {
                //Log.d("filepathUrl",""+error);
            }
        });
    }
}