package com.info.collab.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.info.collab.R;
import com.info.collab.modalClass.ChatMD;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.Constant;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatingAdapter extends RecyclerView.Adapter<ChatingAdapter.ImageViewHolder> {
    ArrayList<ChatMD> imageArrayList;
    Context ctx;

    public ChatingAdapter(Context ctx,ArrayList<ChatMD> imageArrayList) {
        this.imageArrayList = imageArrayList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ImageViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chating_item_row, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int i) {
        ChatMD product = imageArrayList.get(i);

       String uni_id =  AppPreferences.getStringPreference(ctx , Constant.uni_id);

        if (product.getUser2_id()!=null){
            if (uni_id.equalsIgnoreCase(product.getUser1_id())){
                imageViewHolder.reciveTV.setText(product.getMessage()+"\n"+product.getUpdated_at());
                imageViewHolder.name1TV.setText(product.getBusiness_name());
                Glide.with(imageViewHolder.user1Img.getContext()).load(product.getBusiness_logo()).error(R.drawable.test_logo).into(imageViewHolder.user1Img);
                imageViewHolder.layout2.setVisibility(View.GONE);
                imageViewHolder.layout1.setVisibility(View.VISIBLE);
           }else {
                imageViewHolder.sendTV.setText(product.getMessage()+"\n"+product.getUpdated_at());
                imageViewHolder.name2TV.setText(product.getBusiness_name());
                Glide.with(imageViewHolder.user2Img.getContext()).load(product.getBusiness_logo()).error(R.drawable.test_logo).into(imageViewHolder.user2Img);
                imageViewHolder.layout1.setVisibility(View.GONE);
                imageViewHolder.layout2.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return imageArrayList.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder  {
        CircleImageView user1Img,user2Img;
        TextView reciveTV,sendTV,name1TV,name2TV;
        RelativeLayout layout1,layout2;

        public ImageViewHolder(View itemView) {
            super(itemView);
            reciveTV = itemView.findViewById(R.id.reciveTV);
            sendTV = itemView.findViewById(R.id.sendTV);
            name1TV = itemView.findViewById(R.id.name1TV);
            name2TV = itemView.findViewById(R.id.name2TV);
            layout1 = itemView.findViewById(R.id.layout1);
            layout2 = itemView.findViewById(R.id.layout2);
            user1Img = itemView.findViewById(R.id.user1Img);
            user2Img = itemView.findViewById(R.id.user2Img);
        }
    }
}
