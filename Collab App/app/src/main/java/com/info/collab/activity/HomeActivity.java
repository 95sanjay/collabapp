package com.info.collab.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.info.collab.R;
import com.info.collab.fragements.AccountFragment;
import com.info.collab.fragements.BaseFragment;
import com.info.collab.fragements.CollabFragment;
import com.info.collab.fragements.FeedFragment;
import com.info.collab.fragements.HomeFragment;
import com.info.collab.test.HttpsTrustManager;

public class HomeActivity extends AppCompatActivity{
    private BaseFragment openFragment;
    private BottomNavigationView navigation;
    private Toolbar toolbar;
    FrameLayout frame_container;
    public static FragmentTransaction fragmentTransaction;
    public static FragmentManager fragmentManager;

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        setContentView(R.layout.activity_home);
        HttpsTrustManager.allowAllSSL();
        Init();
    }

    @SuppressLint("WrongViewCast")
    void Init() {
        navigation = findViewById(R.id.navigation);
        toolbar = findViewById(R.id.top_toolbar);
        frame_container = findViewById(R.id.frame_container);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.home);
        ((TextView) toolbar.findViewById(R.id.title)).setText(getResources().getString(R.string.app_name));
        fragmentManager = getSupportFragmentManager();
        changeFragment(R.id.home, null,false);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            changeFragment(item.getItemId(), null,false);
            return true;
        }
    };

    public void changeFragment(int id, Bundle bundle,boolean isBackStack) {
        if (isBackStack)
            clearBackStack();
        switch (id) {
            case R.id.home:
                if (!(openFragment instanceof HomeFragment)) {
                    ((TextView) toolbar.findViewById(R.id.title)).setVisibility(View.GONE);
                    toolbar.findViewById(R.id.edt_search).setVisibility(View.VISIBLE);
                    openFragment = HomeFragment.newInstance(bundle);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_container, openFragment);
                    transaction.disallowAddToBackStack();
                    transaction.commit();
                }
                break;
            case R.id.feed:
                if (!(openFragment instanceof FeedFragment)) {
                    ((TextView) toolbar.findViewById(R.id.title)).setText(getResources().getString(R.string.title_fav));
                    ImageView add = (ImageView) toolbar.findViewById(R.id.addBtn);
                    add.setVisibility(View.VISIBLE);
                    add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(getApplicationContext(), AddProductActivity.class);
                            startActivity(intent);
                        }
                    });
                    openFragment = FeedFragment.newInstance(bundle);
                    loadFragment(openFragment);
                }
                break;
            case R.id.collab:
                if (!(openFragment instanceof CollabFragment)) {
                    ((TextView) toolbar.findViewById(R.id.title)).setText(getResources().getString(R.string.app_name));
                    openFragment = CollabFragment.newInstance(bundle);
                    loadFragment(openFragment);
                }
                break;
            case R.id.account:
                if (!(openFragment instanceof AccountFragment)) {
                    ((TextView) toolbar.findViewById(R.id.title)).setText(getResources().getString(R.string.title_inbox));
                    openFragment = AccountFragment.newInstance(bundle);
                    loadFragment(openFragment);
                }
                break;
        }
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    private void clearBackStack() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }
}