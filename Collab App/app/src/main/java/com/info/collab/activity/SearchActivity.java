package com.info.collab.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.info.collab.R;
import com.info.collab.adapter.SearchAdapter;
import com.info.collab.adapter.SearchServiceAdapter;
import com.info.collab.modalClass.Product;
import com.info.collab.modalClass.ServiceMD;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.BaseActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class SearchActivity extends BaseActivity implements View.OnClickListener{

    private RecyclerView feed_rv;
    private ArrayList<Product> productslist;
    private ArrayList<ServiceMD> serviceMDArrayList;
    private SearchAdapter productAdapter;
    private ImageView addBtn,backBtn;
    private SearchView edt_search;
    private LinearLayout filter_layout;
    private SearchServiceAdapter serviceAdapter;

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_list);
        setStatusBarGradiant(this);
        init();
    }

    void init(){
        feed_rv = findViewById(R.id.feed_rv);
        backBtn = findViewById(R.id.backBtn);
        addBtn = findViewById(R.id.addBtn);
        edt_search = findViewById(R.id.edt_search);
        filter_layout = findViewById(R.id.filter_layout);
        filter_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBox();
            }
        });
        backBtn.setOnClickListener(this);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SearchActivity.this, AddProductActivity.class);
                startActivity(intent);
            }
        });
        productApi();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.backBtn:
                onBackPressed();
                break;
        }
    }

    private void productApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("filter_by", "product");
        RequestBody requestBody = builder.build();
        RetrofitService.Filter(new Dialog(SearchActivity.this), retrofitApiClient.Filter(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;

                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");

                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("product list");
                        JSONObject object = null;
                        productslist = new ArrayList<>();
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            object = jsonArray.getJSONObject(i);
                            Product product = new Product();
                            product.setProduct_identifier(object.getString("product_identifier"));
                            product.setBID(object.getString("BID"));
                            product.setHs_code(object.getString("hs_code"));
                            product.setSeller_type(object.getString("seller_type"));
                            product.setArabic_name(object.getString("arabic_name"));
                            product.setEnglish_name(object.getString("english_name"));
                            product.setProduct_category_id(object.getString("product_category_id"));
                            product.setProduct_sub_category_id(object.getString("product_sub_category_id"));
                            product.setProduct_description(object.getString("product_description"));
                            product.setProduct_main_image(object.getString("product_main_image"));
                            product.setProduct_creation_date(object.getString("product_creation_date"));
                            JSONArray jsonArray1 = object.getJSONArray("created_by");
                            for (int j = 0 ; j < jsonArray1.length() ; j++){
                                JSONObject object1 = jsonArray1.getJSONObject(j);
                                product.setBusiness_address(object1.getString("business_address"));
                            }
                            productslist.add(product);
                        }

                        productAdapter = new SearchAdapter(productslist, new SearchAdapter.OnItemNewInClick() {
                            @Override
                            public void onItemClick(Product item) {
                                Intent intent = new Intent(getApplicationContext(), ProductInfo.class);
                                intent.putExtra("product_id",item.getProduct_identifier());
                                startActivity(intent);
                            }
                        });
                        feed_rv.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                        feed_rv.setItemAnimator(new DefaultItemAnimator());
                        feed_rv.setAdapter(productAdapter);
                        productAdapter.notifyDataSetChanged();

                        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
                        edt_search.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
                        edt_search.setMaxWidth(Integer.MAX_VALUE);
                        edt_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                            @Override
                            public boolean onQueryTextSubmit(String query) {
                                productAdapter.getFilter().filter(query);
                                return false;
                            }

                            @Override
                            public boolean onQueryTextChange(String newText) {
                                productAdapter.getFilter().filter(newText);
                                return false;
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void serviceApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("filter_by", "service");
        RequestBody requestBody = builder.build();
        RetrofitService.Filter(new Dialog(SearchActivity.this), retrofitApiClient.Filter(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;

                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");

                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("service list");
                        JSONObject object = null;
                        serviceMDArrayList = new ArrayList<>();
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            object = jsonArray.getJSONObject(i);
                            ServiceMD product = new ServiceMD();
                            product.setId(object.getString("id"));
                            product.setCreated_by(object.getString("created_by"));
                            product.setEnglish_name(object.getString("english_name"));
                            product.setService_description(object.getString("service_description"));
                            product.setService_main_image(object.getString("service_main_image"));
                            product.setCategory(object.getString("category"));
                            product.setCreated_date_time(object.getString("created_date_time"));
                            JSONArray jsonArray1 = object.getJSONArray("created_by");
                            for (int j = 0 ; j < jsonArray1.length(); j++){
                                JSONObject object1 = jsonArray1.getJSONObject(j);
                                product.setBusiness_name(object1.getString("business_name"));
                                product.setBusiness_address(object1.getString("business_address"));
                                product.setCreated_by_id(object1.getString("uni_id"));
                            }
                            serviceMDArrayList.add(product);
                        }

                        serviceAdapter = new SearchServiceAdapter(serviceMDArrayList, new SearchServiceAdapter.OnItemNewInClick() {
                            @Override
                            public void onItemClick(ServiceMD item) {
                                Intent intent = new Intent(getApplicationContext(), ServiceInfo.class);
                                intent.putExtra("service",item);
                                startActivity(intent);
                                //serviceBox(item);
                            }
                        });
                        feed_rv.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                        feed_rv.setItemAnimator(new DefaultItemAnimator());
                        feed_rv.setAdapter(serviceAdapter);
                        serviceAdapter.notifyDataSetChanged();

                        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
                        edt_search.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
                        edt_search.setMaxWidth(Integer.MAX_VALUE);
                        edt_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                            @Override
                            public boolean onQueryTextSubmit(String query) {
                                serviceAdapter.getFilter().filter(query);
                                return false;
                            }

                            @Override
                            public boolean onQueryTextChange(String newText) {
                                serviceAdapter.getFilter().filter(newText);
                                return false;
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    public void dialogBox(){
        final String[] fonts = {"Product", "Service"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Search");
        builder.setItems(fonts, (dialog, which) -> {
            if ("Product".equals(fonts[which])) {
                productApi();
            } else if ("Service".equals(fonts[which])) {
                serviceApi();
            }
        });
        builder.show();
    }

    public void serviceBox(ServiceMD service){
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.filter_dialog);
        TextView txt = dialog.findViewById(R.id.messagetv);
        TextView title = dialog.findViewById(R.id.title);
        ImageView image = dialog.findViewById(R.id.image);
        txt.setText(service.getService_description());
        title.setText(service.getEnglish_name());
        Glide.with(image).load(service.getService_main_image()).into(image);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        Button declineButton = (Button) dialog.findViewById(R.id.btnDialogCancel);
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button videoButton = (Button) dialog.findViewById(R.id.btnDialogOk);
        videoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
}