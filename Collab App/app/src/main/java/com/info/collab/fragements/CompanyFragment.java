package com.info.collab.fragements;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.info.collab.R;
import com.info.collab.activity.CompanyProfileActivity;
import com.info.collab.activity.ProductInfo;
import com.info.collab.adapter.ChatAdapter;
import com.info.collab.adapter.NewInAdapter;
import com.info.collab.adapter.SubCategoryAdapter;
import com.info.collab.modalClass.CollabMD;
import com.info.collab.modalClass.Feed;
import com.info.collab.modalClass.Product;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class CompanyFragment extends BaseFragment {
    RecyclerView chat_rv;
    private View view;
    private ArrayList<Feed> chatArrayList = new ArrayList<>();
    private ChatAdapter feedAdapter;
    private ArrayList<Product> productslist;
    private SearchView edt_search;
    private NewInAdapter productAdapter;
    private ArrayList<CollabMD> subcategourylist;
    private SubCategoryAdapter subCategoryAdapter;

    public static CompanyFragment newInstance(Bundle args) {
        CompanyFragment fragment = new CompanyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.new_in_fragment, container, false);
        init();
        return view;
    }

    void init(){
        chat_rv = view.findViewById(R.id.chat_rv);
        edt_search = view.findViewById(R.id.edt_search);
        ImageView back_btn = view.findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        TextView title = view.findViewById(R.id.title);
        title.setText("Collab");
        CompanyApi();
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        edt_search.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        edt_search.setMaxWidth(Integer.MAX_VALUE);
        edt_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                subCategoryAdapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                subCategoryAdapter.getFilter().filter(s);
                return false;
            }
        });
    }

    private void CompanyApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(getContext(), Constant.uni_id ))
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(getContext(), Constant.token_id ));
        RequestBody requestBody = builder.build();
        RetrofitService.Collab_list(new Dialog(getContext()), retrofitApiClient.Collab_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("data");
                        JSONObject object = null;
                        subcategourylist = new ArrayList<>();
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            object = jsonArray.getJSONObject(i);
                            CollabMD product = new CollabMD();
                            product.setUni_id(object.getString("uni_id"));
                            product.setBusiness_email(object.getString("business_email"));
                            product.setBusiness_main_phone_number(object.getString("business_main_phone_number"));
                            product.setBusiness_address(object.getString("business_address"));
                            product.setBusiness_name(object.getString("business_name"));
                            product.setBusiness_web_site(object.getString("business_web_site"));
                            product.setBusiness_owner(object.getString("business_owner"));
                            product.setBusiness_bio(object.getString("business_bio"));
                            product.setBusiness_logo(object.getString("business_logo"));
                            product.setBusiness_cover_image(object.getString("business_cover_image"));
                            product.setBusiness_country(object.getString("business_country"));
                            product.setBusiness_country_code(object.getString("business_country_code"));
                            product.setBusiness_city(object.getString("business_city"));
                            product.setBusiness_main_activity_id(object.getString("business_main_activity_id"));
                            product.setBusiness_sub_activity_id(object.getString("business_sub_activity_id"));
                            product.setShelf_doc(object.getString("shelf_doc"));
                            subcategourylist.add(product);
                        }
                        subCategoryAdapter = new SubCategoryAdapter(subcategourylist, item -> {
                            AppPreferences.setStringPreference(appContext, Constant.collab_uni_id, item.getUni_id());
                            Intent intent = new Intent(getContext(), CompanyProfileActivity.class);
                            intent.putExtra("Collab", item);
                            startActivity(intent);
                        });
                        chat_rv.setLayoutManager(new GridLayoutManager(getContext(), 3));
                        chat_rv.setItemAnimator(new DefaultItemAnimator());
                        chat_rv.setAdapter(subCategoryAdapter);
                        subCategoryAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseFailed(String error) {
                //Log.d("filepathUrl",""+error);
            }
        });
    }
}