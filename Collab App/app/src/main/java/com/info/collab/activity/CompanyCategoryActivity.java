package com.info.collab.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.info.collab.R;
import com.info.collab.adapter.SearchAdapter;
import com.info.collab.adapter.companyAdapter;
import com.info.collab.modalClass.CollabMD;
import com.info.collab.modalClass.CompanyMD;
import com.info.collab.modalClass.Product;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.BaseActivity;
import com.info.collab.utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class CompanyCategoryActivity extends BaseActivity implements View.OnClickListener{

    RecyclerView feed_rv;
    private ArrayList<CollabMD> productslist;
    private companyAdapter productAdapter;
    ImageView addBtn,backBtn;
    SearchView edt_search;
    private String category_id;
    TextView title;
    CardView search_cv;
    CardView filter_card;

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_list2);
        category_id = getIntent().getExtras().getString("business_category_id");
        setStatusBarGradiant(this);
        init();
    }

    void init(){
        feed_rv = findViewById(R.id.feed_rv);
        backBtn = findViewById(R.id.backBtn);
        addBtn = findViewById(R.id.addBtn);
        filter_card = findViewById(R.id.filter_card);
        title = findViewById(R.id.title);
        edt_search = findViewById(R.id.edt_search);
        title.setText("Collab");
        filter_card.setVisibility(View.GONE);
        backBtn.setOnClickListener(this);
        addBtn.setOnClickListener(view -> {
            Intent intent = new Intent(CompanyCategoryActivity.this, AddProductActivity.class);
            startActivity(intent);
        });
        productApi();

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        edt_search.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        edt_search.setMaxWidth(Integer.MAX_VALUE);
        edt_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                productAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                productAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.backBtn:
                onBackPressed();
                break;
        }
    }

    private void productApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(CompanyCategoryActivity.this, Constant.uni_id ))
                .addFormDataPart("business_category_id", category_id);

        RequestBody requestBody = builder.build();
        RetrofitService.Business_list(new Dialog(CompanyCategoryActivity.this), retrofitApiClient.Business_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("data");
                        JSONObject object = null;
                        productslist = new ArrayList<>();
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            object = jsonArray.getJSONObject(i);
                            CollabMD product = new CollabMD();
                            product.setUni_id(object.getString("uni_id"));
                            product.setBusiness_email(object.getString("business_email"));
                            product.setBusiness_main_phone_number(object.getString("business_main_phone_number"));
                            product.setBusiness_address(object.getString("business_address"));
                            product.setBusiness_name(object.getString("business_name"));
                            product.setBusiness_web_site(object.getString("business_web_site"));
                            product.setBusiness_owner(object.getString("business_owner"));
                            product.setBusiness_bio(object.getString("business_bio"));
                            product.setBusiness_logo(object.getString("business_logo"));
                            product.setBusiness_cover_image(object.getString("business_cover_image"));
                            product.setBusiness_country(object.getString("business_country"));
                            product.setBusiness_country_code(object.getString("business_country_code"));
                            product.setBusiness_city(object.getString("business_city"));
                            product.setBusiness_main_activity_id(object.getString("business_main_activity_id"));
                            product.setBusiness_sub_activity_id(object.getString("business_sub_activity_id"));
                            product.setShelf_doc(object.getString("shelf_doc"));
                            productslist.add(product);
                        }
                        productAdapter = new companyAdapter(productslist, item -> {
                            AppPreferences.setStringPreference(CompanyCategoryActivity.this, Constant.collab_uni_id, item.getUni_id());
                            Intent intent = new Intent(getApplicationContext(), CompanyProfileActivity.class);
                            intent.putExtra("Collab",item);
                            startActivity(intent);
                        });
                        feed_rv.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                        feed_rv.setItemAnimator(new DefaultItemAnimator());
                        feed_rv.setAdapter(productAdapter);
                        productAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }
}