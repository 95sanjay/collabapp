package com.info.collab.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.info.collab.R;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.Alerts;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.BaseActivity;
import com.info.collab.utils.Constant;
import com.info.collab.utils.EmailChecker;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class UpdateProfileActivity extends BaseActivity implements View.OnClickListener {
    private Context appContext;
    TextView btn_sign_in;
    EditText edt_email, phone_edt, b_name_edt, location_edt,bio_edt;
    ImageView auth_doc_Img,profile_Img,banner_Img;
    ImageView pick_location;
    private static int AUTOCOMPLETE_REQUEST_CODE = 1;

    public final int REQUEST_CODE_PICKER = 125;
    public final int REQUEST_CODE_PICKER2 = 12256;
    public final int REQUEST_CODE_PICKER3 = 12236556;
    ArrayList<Image> images = new ArrayList<>();
    ArrayList<Image> images2 = new ArrayList<>();
    ArrayList<Image> images3 = new ArrayList<>();

    private byte[] imageBytes3;
    private String timeStamp3="";
    private String extension3="";

    private byte[] imageBytes1;
    private String timeStamp1="";
    private String extension1="";

    private byte[] imageBytes2;
    private String timeStamp2="";
    private String extension2="";

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);

    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        appContext = this;
        setContentView(R.layout.update_profile);
        SetupUI();
    }

    private void SetupUI(){
        edt_email = findViewById(R.id.edt_email);
        phone_edt = findViewById(R.id.phone_edt);
        b_name_edt = findViewById(R.id.b_name_edt);
        location_edt = findViewById(R.id.location_edt);
        bio_edt = findViewById(R.id.bio_edt);
        btn_sign_in = findViewById(R.id.btn_sign_in);
        pick_location = findViewById(R.id.pick_location);
        banner_Img = findViewById(R.id.banner_Img);
        profile_Img = findViewById(R.id.profile_Img);
        auth_doc_Img = findViewById(R.id.auth_doc_Img);
        banner_Img.setOnClickListener(this);
        profile_Img.setOnClickListener(this);
        auth_doc_Img.setOnClickListener(this);

        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             /*   if (isInputValid()) {
                *//*Intent intent = new Intent(UpdateProfileActivity.this,Signup2Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("email", edt_email.getText().toString().trim());
                bundle.putString("phone", phone_edt.getText().toString().trim());
                bundle.putString("b_name", b_name_edt.getText().toString().trim());
                bundle.putString("location", location_edt.getText().toString().trim());
                bundle.putString("pass", bio_edt.getText().toString().trim());
                intent.putExtras(bundle);
                startActivity(intent);*//*
               }*/
                updateprofile();
            }
        });
    }

    private boolean isInputValid() {
        if (edt_email.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter email.");
            return false;
        } else if (!EmailChecker.checkEmail(edt_email.getText().toString().trim())) {
            Alerts.showToast(this, "Please enter valid email.");
            return false;
        } else if (phone_edt.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter phone number.");
            return false;
        } else if (b_name_edt.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter business name.");
            return false;
        }else if (location_edt.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter location.");
            return false;
        }else if (bio_edt.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter Bio.");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.auth_doc_Img:
                selectauth();
                break;

            case R.id.profile_Img:
                selectlogo();
                break;

            case R.id.banner_Img:
                selectBanner();
                break;

            case R.id.btn_sign_in2:
                /*if (isInputValid()) {
                   // uploadImagenew();
                }*/
                updateprofile();
                break;
        }
    }


    private void updateprofile(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("name", b_name_edt.getText().toString().trim())
                .addFormDataPart("username", edt_email.getText().toString().trim())
                .addFormDataPart("email_id", edt_email.getText().toString().trim())
                .addFormDataPart("phone_no", phone_edt.getText().toString().trim())
                .addFormDataPart("address", location_edt.getText().toString().trim())
                .addFormDataPart("bio",bio_edt.getText().toString().trim())
                .addFormDataPart("ownername", AppPreferences.getStringPreference(this,Constant.business_owner))
                .addFormDataPart("password", AppPreferences.getStringPreference(this,Constant.business_password))
                .addFormDataPart("doc_type", AppPreferences.getStringPreference(this,Constant.doc_type))
                .addFormDataPart("country", AppPreferences.getStringPreference(this,Constant.business_country))
                .addFormDataPart("country_code", AppPreferences.getStringPreference(this,Constant.business_country_code))
                .addFormDataPart("city", AppPreferences.getStringPreference(this,Constant.business_city))
                .addFormDataPart("main_activity", AppPreferences.getStringPreference(this,Constant.business_main_activity_id))
                .addFormDataPart("sub_activity", AppPreferences.getStringPreference(this,Constant.business_sub_activity_id))
                .addFormDataPart("creation_date", AppPreferences.getStringPreference(this,Constant.business_creation_date))
                .addFormDataPart("expiry_date", AppPreferences.getStringPreference(this,Constant.business_exp_date))
                .addFormDataPart("user_id", AppPreferences.getStringPreference(this,Constant.uni_id))
                .addFormDataPart("device_id", "updated"+AppPreferences.getStringPreference(this,Constant.device_id));

        if (images.size()>0){
            File featured_image = new File(images.get(0).getPath());
            if (featured_image.exists()) {
                builder.addFormDataPart("cover_img", featured_image.getName(), RequestBody.create(MultipartBody.FORM, featured_image));
            }
        }
        if (images2.size()>0){
            File featured_image = new File(images2.get(0).getPath());
            if (featured_image.exists()) {
                builder.addFormDataPart("logo", featured_image.getName(), RequestBody.create(MultipartBody.FORM, featured_image));
            }
        }
        if (images3.size()>0){
            File featured_image = new File(images3.get(0).getPath());
            if (featured_image.exists()) {
                builder.addFormDataPart("shelf_doc", featured_image.getName(), RequestBody.create(MultipartBody.FORM, featured_image));
                builder.addFormDataPart("auth_doc[]", featured_image.getName(), RequestBody.create(MultipartBody.FORM, featured_image));
            }
        }

        RequestBody requestBody = builder.build();
        RetrofitService.Edit_user(new Dialog(UpdateProfileActivity.this), retrofitApiClient.Edit_user(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson2(response);
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void parseJson2(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject jsonObject = new JSONObject(response.body().string());
             Log.d("business_Sanjy",""+jsonObject);
            if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0; i < jsonArray.length() ; i++){
                    JSONObject object = jsonArray.getJSONObject(i);
                    AppPreferences.setStringPreference(appContext, Constant.business_email, object.getString(Constant.business_email));
                    AppPreferences.setStringPreference(appContext, Constant.business_main_phone_number, object.getString(Constant.business_main_phone_number));
                    AppPreferences.setStringPreference(appContext, Constant.business_address, object.getString(Constant.business_address));
                    AppPreferences.setStringPreference(appContext, Constant.business_name, object.getString(Constant.business_name));
                    AppPreferences.setStringPreference(appContext, Constant.business_username, object.getString(Constant.business_username));
                    AppPreferences.setStringPreference(appContext, Constant.business_bio, object.getString(Constant.business_bio));
                  //  AppPreferences.setStringPreference(appContext, Constant.authentic_doc, object.getString(Constant.authentic_doc));
                  //  AppPreferences.setStringPreference(appContext, Constant.shelf_doc, object.getString(Constant.shelf_doc));
                  //  AppPreferences.setStringPreference(appContext, Constant.business_logo, object.getString(Constant.business_logo));
                  //  AppPreferences.setStringPreference(appContext, Constant.business_cover_image, object.getString(Constant.business_cover_image));
                }
                b_name_edt.setText("");
                edt_email.setText("");
                bio_edt.setText("");
                phone_edt.setText("");
                location_edt.setText("");
                Alerts.showToast(UpdateProfileActivity.this,jsonObject.getString("message"));
            }else {
                Alerts.showToast(UpdateProfileActivity.this,jsonObject.getString("message"));
            }
        } catch (Exception e) {

        }
    }

    void selectBanner() {
        ImagePicker.create(UpdateProfileActivity.this)
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .theme(R.style.ImagePickerTheme)
                .single() // max images can be selected (99 by default)
                .showCamera(false) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image ("Camera" folder by default)
                .origin(images) // original selected images, used in multi mode
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }

    void selectlogo() {
        ImagePicker.create(UpdateProfileActivity.this)
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .theme(R.style.ImagePickerTheme)
                .single() // max images can be selected (99 by default)
                .showCamera(false) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image ("Camera" folder by default)
                .origin(images2) // original selected images, used in multi mode
                .start(REQUEST_CODE_PICKER2); // start image picker activity with request code
    }

    void selectauth() {
        ImagePicker.create(UpdateProfileActivity.this)
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .theme(R.style.ImagePickerTheme)
                .single() // max images can be selected (99 by default)
                .showCamera(false) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image ("Camera" folder by default)
                .origin(images3) // original selected images, used in multi mode
                .start(REQUEST_CODE_PICKER3); // start image picker activity with request code
    }

    @Override
    public void onActivityResult(int requestCode, final int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICKER && resultCode == Activity.RESULT_OK && data != null) {
            images.clear();
            images.addAll(ImagePicker.getImages(data));
            if (images.size() > 0) {

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeFile(images.get(0).getPath());
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                imageBytes1 = baos.toByteArray();
                String imageString = Base64.encodeToString(imageBytes1, Base64.DEFAULT);
                Long tsLong = System.currentTimeMillis() / 1000;
                timeStamp1 = tsLong.toString();
                String filenameArray[] = images.get(0).getName().split("\\.");
                extension1 = filenameArray[filenameArray.length - 1];

                banner_Img.setImageBitmap(bitmap);
            }
        } else if (requestCode == REQUEST_CODE_PICKER2 && resultCode == Activity.RESULT_OK && data != null) {
            images2.clear();
            images2.addAll(ImagePicker.getImages(data));
            if (images2.size() > 0) {

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeFile(images2.get(0).getPath());
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                imageBytes2 = baos.toByteArray();
                String imageString = Base64.encodeToString(imageBytes2, Base64.DEFAULT);
                Long tsLong = System.currentTimeMillis() / 1000;
                timeStamp2 = tsLong.toString();
                String filenameArray[] = images2.get(0).getName().split("\\.");
                extension2 = filenameArray[filenameArray.length - 1];

                profile_Img.setImageBitmap(bitmap);
            }
        }else if (requestCode == REQUEST_CODE_PICKER3 && resultCode == Activity.RESULT_OK && data != null) {
            images3.clear();
            images3.addAll(ImagePicker.getImages(data));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Bitmap bitmap = BitmapFactory.decodeFile(images3.get(0).getPath());
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            imageBytes3 = baos.toByteArray();
            String imageString = Base64.encodeToString(imageBytes3, Base64.DEFAULT);
            Long tsLong = System.currentTimeMillis() / 1000;
            timeStamp3 = tsLong.toString();
            String filenameArray[] = images3.get(0).getName().split("\\.");
            extension3 = filenameArray[filenameArray.length - 1];
            auth_doc_Img.setImageBitmap(bitmap);
        }
    }
}
