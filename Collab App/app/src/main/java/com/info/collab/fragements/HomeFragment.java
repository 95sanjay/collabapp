package com.info.collab.fragements;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.info.collab.R;
import com.info.collab.activity.CategoryActivity;
import com.info.collab.activity.ChatActivity;
import com.info.collab.activity.CompanyCategoryActivity;
import com.info.collab.activity.CompanyProfileActivity;
import com.info.collab.activity.ProductInfo;
import com.info.collab.activity.SearchActivity;
import com.info.collab.adapter.CompanyCategoryAdapter;
import com.info.collab.adapter.ProductAdapter;
import com.info.collab.adapter.ProductCategoryAdapter;
import com.info.collab.adapter.SubCategoryAdapter;
import com.info.collab.modalClass.CategoryMD;
import com.info.collab.modalClass.Product;
import com.info.collab.modalClass.ProductCategory;
import com.info.collab.modalClass.CollabMD;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.test.HttpsTrustManager;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.Constant;
import com.info.collab.utils.OnItemCategoryClick;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class HomeFragment extends BaseFragment implements View.OnClickListener {

    private View view;
    RecyclerView product_list_rv,category_rv,sub_category_list_rv,business_category_rv;
    ArrayList<Product> productslist = new ArrayList<>();
    ArrayList<ProductCategory> productcategourylist = new ArrayList<>();
    ArrayList<CollabMD> subcategourylist = new ArrayList<>();
    ArrayList<CategoryMD> companyCategoryList = new ArrayList<>();
    private ProductAdapter productAdapter;
    private ProductCategoryAdapter productCategoryAdapterAdapter;
    private SubCategoryAdapter subCategoryAdapter;
    TextView edt_search;
    private TextView show_more1,show_more2,show_more0;
    private SwipeRefreshLayout pullToRefresh;
    private String uni_id;

    public static HomeFragment newInstance(Bundle args) {
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        InitialSetup();
        return view;
    }

    void  InitialSetup(){
        uni_id = AppPreferences.getStringPreference(getContext(), Constant.uni_id );
        product_list_rv = view.findViewById(R.id.product_list_rv);
        category_rv = view.findViewById(R.id.category_rv);
        business_category_rv = view.findViewById(R.id.business_category_rv);
        edt_search = view.findViewById(R.id.edt_search);
        show_more0 = view.findViewById(R.id.show_more0);
        show_more1 = view.findViewById(R.id.show_more1);
        show_more2 = view.findViewById(R.id.show_more2);
        pullToRefresh = view.findViewById(R.id.pullToRefresh);
        show_more0.setOnClickListener(this);
        show_more1.setOnClickListener(this);
        show_more2.setOnClickListener(this);
        edt_search.setOnClickListener(this);
        sub_category_list_rv = view.findViewById(R.id.sub_category_list_rv);
        HttpsTrustManager.allowAllSSL();
        productApi();
        productCategouryApi();
        businessApi();
        subCategouryApi();

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                productApi();
                productCategouryApi();
                businessApi();
                subCategouryApi();
                pullToRefresh.setRefreshing(false);
            }
        });
    }

    private void productApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", uni_id)
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(getContext(), Constant.token_id ));
        RequestBody requestBody = builder.build();
        RetrofitService.NewInProduct_list(new Dialog(getContext()), retrofitApiClient.NewInProduct_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("product_list");
                        JSONObject object = null;
                        productslist = new ArrayList<>();

                        if (jsonArray.length() >= 20){
                            for (int i = 0 ; i < 20 ; i++){
                                object = jsonArray.getJSONObject(i);
                                Product product = new Product();
                                product.setProduct_identifier(object.getString("product_identifier"));
                                product.setBID(object.getString("BID"));
                                product.setHs_code(object.getString("hs_code"));
                                product.setCreated_by(object.getString("created_by"));
                                product.setSeller_type(object.getString("seller_type"));
                                product.setArabic_name(object.getString("arabic_name"));
                                product.setEnglish_name(object.getString("english_name"));
                                product.setProduct_category_id(object.getString("product_category_id"));
                                product.setProduct_sub_category_id(object.getString("product_sub_category_id"));
                                product.setProduct_description(object.getString("product_description"));
                                product.setProduct_main_image(object.getString("product_main_image"));
                                product.setProduct_creation_date(object.getString("product_creation_date"));
                                productslist.add(product);
                            }
                        }else {
                            for (int i = 0 ; i < jsonArray.length() ; i++){
                                object = jsonArray.getJSONObject(i);
                                Product product = new Product();
                                product.setProduct_identifier(object.getString("product_identifier"));
                                product.setBID(object.getString("BID"));
                                product.setHs_code(object.getString("hs_code"));
                                product.setCreated_by(object.getString("created_by"));
                                product.setSeller_type(object.getString("seller_type"));
                                product.setArabic_name(object.getString("arabic_name"));
                                product.setEnglish_name(object.getString("english_name"));
                                product.setProduct_category_id(object.getString("product_category_id"));
                                product.setProduct_sub_category_id(object.getString("product_sub_category_id"));
                                product.setProduct_description(object.getString("product_description"));
                                product.setProduct_main_image(object.getString("product_main_image"));
                                product.setProduct_creation_date(object.getString("product_creation_date"));
                                productslist.add(product);
                            }
                        }
                        productAdapter = new ProductAdapter(getContext(),productslist, item -> {
                            Intent intent = new Intent(getActivity(),ProductInfo.class);
                            intent.putExtra("product_id",item.getProduct_identifier());
                            startActivity(intent);
                        });
                        product_list_rv.setLayoutManager(new LinearLayoutManager(appContext, LinearLayoutManager.HORIZONTAL, false));
                        product_list_rv.setItemAnimator(new DefaultItemAnimator());
                        product_list_rv.setAdapter(productAdapter);
                        productAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseFailed(String error) {
            }
        });
    }

    private void productCategouryApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", uni_id)
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(getContext(), Constant.token_id ));
        RequestBody requestBody = builder.build();
        RetrofitService.Category_list(new Dialog(getContext()), retrofitApiClient.Category_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("category_list");
                        JSONObject object = null;
                        productcategourylist = new ArrayList<>();
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            object = jsonArray.getJSONObject(i);
                            ProductCategory product = new ProductCategory();
                            product.setCategory_identifier(object.getString("category_identifier"));
                            product.setCategory_english_name(object.getString("category_english_name"));
                            product.setCategory_arabic_name(object.getString("category_arabic_name"));
                            product.setCategory_description(object.getString("category_description"));
                            product.setCategory_main_image(object.getString("category_img"));
                            productcategourylist.add(product);
                        }
                        productCategoryAdapterAdapter = new ProductCategoryAdapter(productcategourylist, new OnItemCategoryClick() {
                            @Override
                            public void onItemClick(ProductCategory item) {
                               Intent intent = new Intent(getContext(), CategoryActivity.class);
                               intent.putExtra("category_id",item.getCategory_identifier());
                               startActivity(intent);
                            }
                        });
                        category_rv.setLayoutManager(new LinearLayoutManager(appContext, LinearLayoutManager.HORIZONTAL, false));
                        category_rv.setItemAnimator(new DefaultItemAnimator());
                        category_rv.setAdapter(productCategoryAdapterAdapter);
                        productCategoryAdapterAdapter.notifyDataSetChanged();
                    }else {
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseFailed(String error) {}
        });
    }


    private void subCategouryApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id",uni_id)
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(getContext(), Constant.token_id ));
        RequestBody requestBody = builder.build();
        RetrofitService.Collab_list(new Dialog(getContext()), retrofitApiClient.Collab_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("data");
                        JSONObject object = null;
                        subcategourylist = new ArrayList<>();
                        for (int i = 0 ; i < 12 ; i++){
                            object = jsonArray.getJSONObject(i);
                            CollabMD product = new CollabMD();
                            product.setUni_id(object.getString("uni_id"));
                            product.setBusiness_email(object.getString("business_email"));
                            product.setBusiness_main_phone_number(object.getString("business_main_phone_number"));
                            product.setBusiness_address(object.getString("business_address"));
                            product.setBusiness_name(object.getString("business_name"));
                            product.setBusiness_web_site(object.getString("business_web_site"));
                            product.setBusiness_owner(object.getString("business_owner"));
                            product.setBusiness_bio(object.getString("business_bio"));
                            product.setBusiness_logo(object.getString("business_logo"));
                            product.setBusiness_cover_image(object.getString("business_cover_image"));
                            product.setBusiness_country(object.getString("business_country"));
                            product.setBusiness_country_code(object.getString("business_country_code"));
                            product.setBusiness_city(object.getString("business_city"));
                            product.setBusiness_main_activity_id(object.getString("business_main_activity_id"));
                            product.setBusiness_sub_activity_id(object.getString("business_sub_activity_id"));
                            product.setShelf_doc(object.getString("shelf_doc"));
                            subcategourylist.add(product);
                        }
                        subCategoryAdapter = new SubCategoryAdapter(subcategourylist, item -> {
                            AppPreferences.setStringPreference(appContext, Constant.collab_uni_id, item.getUni_id());
                            Intent intent = new Intent(getContext(), CompanyProfileActivity.class);
                            intent.putExtra("Collab", item);
                            startActivity(intent);
                        });
                        sub_category_list_rv.setLayoutManager(new GridLayoutManager(getContext(), 3));
                        sub_category_list_rv.setItemAnimator(new DefaultItemAnimator());
                        sub_category_list_rv.setAdapter(subCategoryAdapter);
                        subCategoryAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseFailed(String error) {
            }
        });
    }

private void businessApi(){
    MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
    builder.addFormDataPart("user_id",uni_id );
    RequestBody requestBody = builder.build();
    RetrofitService.Company_category_list(new Dialog(getContext()), retrofitApiClient.Company_category_list(requestBody), new WebResponse() {
        @Override
        public void onResponseSuccess(retrofit2.Response<?> result) {
            retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
            try {
                JSONObject resObj = null;
                resObj = new JSONObject(response.body().string());
                String message = resObj.optString("message");
                String status = resObj.optString("status");
                JSONArray jsonArray = null;
                if (status.equals("true")) {
                    jsonArray = resObj.getJSONArray("data");
                    JSONObject object = null;
                    companyCategoryList = new ArrayList<>();
                        for (int i = 0 ; i < 6 ; i++){
                            object = jsonArray.getJSONObject(i);
                            CategoryMD product = new CategoryMD();
                            product.setId(object.getString("id"));
                            product.setName(object.getString("name"));
                            product.setImage(object.getString("image"));
                            companyCategoryList.add(product);
                        }/*else {
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            object = jsonArray.getJSONObject(i);
                            CategoryMD product = new CategoryMD();
                            product.setId(object.getString("id"));
                            product.setName(object.getString("name"));
                            product.setImage(object.getString("image"));
                            companyCategoryList.add(product);
                        }
                    }*/

                    CompanyCategoryAdapter subCategoryAdapter = new CompanyCategoryAdapter(companyCategoryList, item -> {
                        Intent intent = new Intent(getContext(), CompanyCategoryActivity.class);
                        intent.putExtra("business_category_id", item.getId());
                        startActivity(intent);
                    });
                    business_category_rv.setLayoutManager(new GridLayoutManager(getContext(), 3));
                    business_category_rv.setItemAnimator(new DefaultItemAnimator());
                    business_category_rv.setAdapter(subCategoryAdapter);
                    subCategoryAdapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onResponseFailed(String error) {
        }
    });
}

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.edt_search:
                Intent intent = new Intent(getContext(), SearchActivity.class);
                startActivity(intent);
                break;

            case R.id.show_more0:
                loadFragment(new NewInFragment());
                break;

            case R.id.show_more1:
                loadFragment(new CategoryFragment());
                break;

            case R.id.show_more2:
                loadFragment(new CompanyFragment());
                break;
        }
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}