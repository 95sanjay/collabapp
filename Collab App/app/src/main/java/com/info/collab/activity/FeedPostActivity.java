package com.info.collab.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.info.collab.R;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.BaseActivity;
import com.info.collab.utils.Constant;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class FeedPostActivity extends BaseActivity implements View.OnClickListener {

    ImageView add_img,backBtn;
    EditText story_tv;
    TextView btn_post;
    private Dialog mProgressBarHandler;
    public final int REQUEST_CODE_PICKER = 125;
    ArrayList<Image> images = new ArrayList<>();
    byte[] imageBytes;
    String timeStamp="";
    String extension="";

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        setContentView(R.layout.fragment_feed_post);
        init();
    }

    void  init(){
        mProgressBarHandler = new Dialog(this);
        add_img = findViewById(R.id.add_img);
        backBtn = findViewById(R.id.backBtn);
        story_tv = findViewById(R.id.story_tv);
        btn_post = findViewById(R.id.btn_post);

        add_img.setOnClickListener(this);
        backBtn.setOnClickListener(this);
        btn_post.setOnClickListener(this);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });
    }

/*
    private void postApi(){
        Alerts.showDialog(mProgressBarHandler,"Please wait...");
        VolleyMultipartRequest volleyMultipartRequest  = new VolleyMultipartRequest(Request.Method.POST, Constant.BASE_URL+ Constant.post,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        Log.e("regresponse", "" + response.toString());
                        String resultResponse = new String(response.data);
                        try {
                            JSONObject resObj = null;
                            resObj = new JSONObject(resultResponse);
                            Log.e("result", "" + resObj);
                            String message = resObj.optString("message");
                            String status = resObj.optString("status");
                            if (status.equals("true")) {
                                Alerts.hide(mProgressBarHandler);
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }else {
                                Alerts.hide(mProgressBarHandler);
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("errror_message",""+error.toString());
                        Alerts.hide(mProgressBarHandler);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> jsonParams = new HashMap<>();
                jsonParams.put("user_id", AppPreferences.getStringPreference(getApplicationContext(), Constant.uni_id ));
                jsonParams.put("tokenid", AppPreferences.getStringPreference(getApplicationContext(), Constant.token_id ));
                jsonParams.put("feedback",story_tv.getText().toString().trim());
                Log.e("params",""+jsonParams);
                return jsonParams;
            }

            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                try {
                    //  Log.e("params",""+timeStamp+"\n"+extension+"\n"+imageBytes);
                    if (imageBytes.length > 0) {
                        params.put("image", new DataPart(timeStamp + "." + extension, imageBytes, "." + extension));
                    }
                }catch (Exception e){}
                return params;
            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(volleyMultipartRequest);
    }*/

    private void postApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(getApplicationContext(), Constant.uni_id ))
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(getApplicationContext(), Constant.token_id ))
                .addFormDataPart("feedback", story_tv.getText().toString().trim());

        if (images.get(0).getPath()!=null){
            File featured_image = new File(images.get(0).getPath());
            if (featured_image.exists()) {
                builder.addFormDataPart("image", featured_image.getName(), RequestBody.create(MultipartBody.FORM, featured_image));
            }
        }
        RequestBody requestBody = builder.build();
        RetrofitService.feed_post(new Dialog(FeedPostActivity.this), retrofitApiClient.feed_post(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson(response);
            }

            @Override
            public void onResponseFailed(String error) {
            }
        });
    }

    private void parseJson(Response<ResponseBody> response) {
        try {
            JSONObject jsonObject = new JSONObject(response.body().string());
            if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                Toast.makeText(this, ""+jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                setResult(12, intent);
                finish();
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_post:
                postApi();
                break;

            case R.id.add_img:
                selectimg();
                break;
/*
            case R.id.cancelBtn:
                onBackPressed();
                Toast.makeText(this, "click", Toast.LENGTH_SHORT).show();
                break;*/
        }
    }


    void selectimg() {
        ImagePicker.create(this)
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .theme(R.style.ImagePickerTheme)
                .single() // max images can be selected (99 by default)
                .showCamera(false) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image ("Camera" folder by default)
                .origin(images) // original selected images, used in multi mode
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }

    @Override
    public void onActivityResult(int requestCode, final int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICKER && resultCode == Activity.RESULT_OK && data != null) {
            images.clear();
            images.addAll(ImagePicker.getImages(data));
            if (images.size() > 0) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeFile(images.get(0).getPath());
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                imageBytes = baos.toByteArray();
                String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                Long tsLong = System.currentTimeMillis() / 1000;
                timeStamp = tsLong.toString();
                String filenameArray[] = images.get(0).getName().split("\\.");
                extension = filenameArray[filenameArray.length - 1];

                Log.d("image","}}}}}}}}}}===  "+imageBytes+"\n"+timeStamp+"\n"+extension);
                add_img.setImageBitmap(bitmap);
            }
        }
    }
}