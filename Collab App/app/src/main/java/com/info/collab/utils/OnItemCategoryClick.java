package com.info.collab.utils;

import com.info.collab.modalClass.Product;
import com.info.collab.modalClass.ProductCategory;

public interface OnItemCategoryClick {
    void onItemClick(ProductCategory item);
}