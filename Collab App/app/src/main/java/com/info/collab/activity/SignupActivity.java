package com.info.collab.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.info.collab.R;
import com.info.collab.utils.Alerts;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.BaseActivity;
import com.info.collab.utils.Constant;
import com.info.collab.utils.EmailChecker;
import com.info.collab.utils.FetchAddressIntentService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class SignupActivity extends BaseActivity implements OnMapReadyCallback {
    private Context appContext;
    TextView btn_sign_in;
    EditText edt_email, phone_edt, b_name_edt, location_edt, pass_edt, con_pass_edt;
    private GoogleMap mMap;
    private FusedLocationProviderClient mLocationClient;
    private String address;
    ImageView pick_location;
    private static int AUTOCOMPLETE_REQUEST_CODE = 1;

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);

    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        appContext = this;
        setContentView(R.layout.fragment_registration);
        SetupUI();
      //setupAutoCompleteFragment();
    }

    private void SetupUI(){
        edt_email = findViewById(R.id.edt_email);
        phone_edt = findViewById(R.id.phone_edt);
        b_name_edt = findViewById(R.id.b_name_edt);
        location_edt = findViewById(R.id.location_edt);
        pass_edt = findViewById(R.id.pass_edt);
        con_pass_edt = findViewById(R.id.con_pass_edt);
        btn_sign_in = findViewById(R.id.btn_sign_in);
        pick_location = findViewById(R.id.pick_location);
        pick_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                location_edt.setText(address);
            }
        });
        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isInputValid()) {
                Intent intent = new Intent(SignupActivity.this,Signup2Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("email", edt_email.getText().toString().trim());
                bundle.putString("phone", phone_edt.getText().toString().trim());
                bundle.putString("b_name", b_name_edt.getText().toString().trim());
                bundle.putString("location", location_edt.getText().toString().trim());
                bundle.putString("pass", pass_edt.getText().toString().trim());
                bundle.putString("con_pass", con_pass_edt.getText().toString().trim());
                intent.putExtras(bundle);
                startActivity(intent);
               }
            }
        });

        String id = getIMEIDeviceId(SignupActivity.this);
        @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
       // Log.d("imei_id",">>>>>>>>>   "+android_id);
        AppPreferences.setStringPreference(appContext, Constant.device_id,id );
    }

    private boolean isInputValid() {
        if (edt_email.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter email.");
            return false;
        } else if (!EmailChecker.checkEmail(edt_email.getText().toString().trim())) {
            Alerts.showToast(this, "Please enter valid email.");
            return false;
        } else if (phone_edt.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter phone number.");
            return false;
        } else if (b_name_edt.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter business name.");
            return false;
        }else if (location_edt.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter location.");
            return false;
        }else if (pass_edt.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter password.");
            return false;
        }else if (con_pass_edt.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter confirm password.");
            return false;
        }
        return true;
    }

    public String getIMEIDeviceId(Context context) {

        String myuniqueID = null;
        int myversion = Integer.valueOf(android.os.Build.VERSION.SDK);
        if (myversion < 23) {
            WifiManager manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = manager.getConnectionInfo();
            myuniqueID= info.getMacAddress();
            if (myuniqueID== null) {
                TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    return myuniqueID;
                }
                myuniqueID= mngr.getDeviceId();
            }
        }
        else if (myversion > 23 && myversion < 29) {
            TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return myuniqueID;
            }
            myuniqueID= mngr.getDeviceId();
        }
        else
        {
            String androidId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
            myuniqueID= androidId;
        }
        return myuniqueID;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);


        mLocationClient = new FusedLocationProviderClient(this);
        mLocationClient.getLastLocation().addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                Location location = task.getResult();
                LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());
                CameraPosition googlePlex = CameraPosition.builder().target(latLng).zoom(10).bearing(0).tilt(45).build();
                mMap.addMarker(new MarkerOptions().position(latLng));
                mMap.setMaxZoomPreference(18);
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 1000, null);
                getAddress(latLng.latitude, latLng.longitude);
            }
        });
    }

    private void getAddress(double latitude, double longitude) {
        StringBuilder result = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                Log.d("SanjaylatLng",""+address+"\n"+city+"\n"+state +"\n"+country+"\n"+postalCode+"\n"+knownName);
            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }
    }
}
