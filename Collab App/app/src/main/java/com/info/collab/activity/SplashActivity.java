package com.info.collab.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.info.collab.R;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.BaseActivity;
import com.info.collab.utils.Constant;

import org.jsoup.Jsoup;

public class SplashActivity extends BaseActivity {
    private static final int DELAYED_TIME = 2000;
    private Context appContext;
    private String currentVersion;
    private static final int PERMISSION_REQUEST_CODE = 200;
    final String[] PERMISSIONS = {Manifest.permission.INTERNET,Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_WIFI_STATE,Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.FOREGROUND_SERVICE,Manifest.permission.CALL_PRIVILEGED,Manifest.permission.GET_ACCOUNTS_PRIVILEGED};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        setContentView(R.layout.activity_splash);
        appContext = this;
        if (Build.VERSION.SDK_INT >=21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        try {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        }
        catch (Exception e) {}

       /* if (cd.isNetworkAvailable()) {
            new GetVersionCode().execute();
        } else {
            cd.showAlert();
        }*/

        requestP();
    }

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    public void requestP() {
        if(!hasPermissions(this, PERMISSIONS))
        {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQUEST_CODE);
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED)
                {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (cd.isNetworkAvailable()) {
                            if(AppPreferences.getBooleanPreference(appContext, Constant.IS_LOGIN)){
                                startActivity(new Intent(appContext,HomeActivity.class));
                                finish();
                            }else {
                                startActivity(new Intent(appContext, SignInActivity.class));
                                finish();
                            }
                        } else {
                            Toast.makeText(appContext, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, 3000);
            } else {
            }
        }
    }
    class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String newVersion = null;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + SplashActivity.this.getPackageName()+ "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();


            } catch (Exception e) {
                e.printStackTrace();
            }
            return newVersion;
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            onlineVersion = "1.0";
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (currentVersion.equals(onlineVersion) ) {
                    if (cd.isNetworkAvailable()) {
                        if (Build.VERSION.SDK_INT >22) {
                            requestP();
                        }
                        else {
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if(AppPreferences.getBooleanPreference(appContext, Constant.IS_LOGIN)){
                                        startActivity(new Intent(appContext,HomeActivity.class));
                                        finish();
                                    }else {
                                        startActivity(new Intent(appContext, SignInActivity.class));
                                        finish();
                                    }
                                }
                            }, 10);
                        }
                    }
                    else {
                             cd.showAlert();
                          }
                }
                else {
                    Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id="+SplashActivity.this.getPackageName()));
                    startActivity(viewIntent);
                    SplashActivity.this.finish();
                }
            }
            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);
        }
    }
}

//https://docs.google.com/document/d/1KpR81Ecw64Yytwk7X3QO0ZMyvQ8Vr5kcSPfj_xGVoZU/edit
//https://www.figma.com/file/iGPdTn40iwf10nJfRrOoLE/Collab_App?node-id=977%3A2
