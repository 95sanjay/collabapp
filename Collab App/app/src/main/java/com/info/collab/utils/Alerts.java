package com.info.collab.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.info.collab.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Lenovo on 28/05/2017.
 */

public class Alerts {

    public static void showToast(Context mContext, String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }
/*
    public static void showSnacbar(View container, String msg, String btn, View.OnClickListener listener) {

        Snackbar snackbar = Snackbar
                .make(container, msg, Snackbar.LENGTH_INDEFINITE)
                .setAction(btn, listener);
        snackbar.show();
    }*/


    public static void showDialog(Dialog mProgressDialog, String msg) {
        try {
            if (mProgressDialog.isShowing())
                return;
            mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mProgressDialog.setContentView(R.layout.layout_progress_bar1);
            ((TextView) mProgressDialog.findViewById(R.id.title)).setText(msg);
            mProgressDialog.setCancelable(false);
            //mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            mProgressDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hide(Dialog mProgressDialog) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public static void alertDialog(Context context, String title, String msg, String btnPos, String btnNag, Dialog.OnClickListener positive, Dialog.OnClickListener nagative) {
        new AlertDialog.Builder(context).setTitle(title)
                .setMessage(msg)
                .setPositiveButton(btnPos, positive)
                .setNegativeButton(btnNag, nagative).create().show();
    }
}
