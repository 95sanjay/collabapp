package com.info.collab.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.info.collab.R;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.Alerts;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.BaseActivity;
import com.info.collab.utils.Constant;
import com.info.collab.utils.EmailChecker;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class SignInActivity extends BaseActivity implements View.OnClickListener {
    private Context appContext;
    EditText edt_email;
    EditText edt_pass;
    String loginMode = "direct";

    TextView btn_forgot;
    TextView btn_sign_in;

    String fbId = "";
    String userEmail = "";
    String userPassword = "";

    AlertDialog.Builder alert;
    private TextView lay_sign_up;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        appContext = this;
        setContentView(R.layout.activity_sign_in);
        Init();
    }

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    void Init() {
        edt_email = findViewById(R.id.edt_email);
        edt_pass = findViewById(R.id.edt_pass);
        btn_sign_in = findViewById(R.id.btn_sign_in);
        lay_sign_up = findViewById(R.id.lay_sign_up);
        btn_forgot = findViewById(R.id.btn_forgot);
        AppCompatImageView collab_img = findViewById(R.id.collab_img);
        btn_sign_in.setOnClickListener(this);
        lay_sign_up.setOnClickListener(this);
        btn_forgot.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lay_sign_up:
                startActivity(new Intent(appContext, SignupActivity.class));
                break;

            case R.id.btn_sign_in:
                if (isInputValid()) {
                    userEmail = edt_email.getText().toString().trim();
                    userPassword = edt_pass.getText().toString().trim();
                    signInServiceCall();
                }
                break;

            case R.id.btn_forgot:
                Intent intent = new Intent(SignInActivity.this , ForgotActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void signInServiceCall() {
        Map<String, Object> jsonParams = new HashMap<>();
        jsonParams.put("email_id",userEmail  );
        jsonParams.put("password", userPassword);

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (new JSONObject(jsonParams)).toString());

        RetrofitService.signIn(new Dialog(appContext), retrofitApiClient.signIn(body), new WebResponse() {
            @Override
            public void onResponseSuccess(Response<?> result) {
                Response<ResponseBody> response = (Response<ResponseBody>) result;
                parseJson(response);
            }

            @Override
            public void onResponseFailed(String error) {
                Alerts.showToast(appContext, error);
            }
        });
    }

    private void parseJson(Response<ResponseBody> response) {
        try {
            JSONObject jsonObject = new JSONObject(response.body().string());

            if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                JSONObject object = jsonObject.getJSONObject("data");
                String userData = new Gson().toJson(object);
                Log.d("ChatActivity","user1_id     "+object.getString(Constant.uni_id));
                AppPreferences.setStringPreference(appContext, Constant.uni_id, object.getString(Constant.uni_id));
                AppPreferences.setStringPreference(appContext, Constant.BID, object.getString(Constant.BID));
                AppPreferences.setStringPreference(appContext, Constant.business_email, object.getString(Constant.business_email));
                AppPreferences.setStringPreference(appContext, Constant.business_main_phone_number, object.getString(Constant.business_main_phone_number));
                AppPreferences.setStringPreference(appContext, Constant.business_address, object.getString(Constant.business_address));
                AppPreferences.setStringPreference(appContext, Constant.business_lat, object.getString(Constant.business_lat));
                AppPreferences.setStringPreference(appContext, Constant.business_lng, object.getString(Constant.business_lng));
                AppPreferences.setStringPreference(appContext, Constant.business_web_site, object.getString(Constant.business_web_site));
                AppPreferences.setStringPreference(appContext, Constant.business_name, object.getString(Constant.business_name));
                AppPreferences.setStringPreference(appContext, Constant.business_username, object.getString(Constant.business_username));
                AppPreferences.setStringPreference(appContext, Constant.business_owner, object.getString(Constant.business_owner));
                AppPreferences.setStringPreference(appContext, Constant.business_password, object.getString(Constant.business_password));
                AppPreferences.setStringPreference(appContext, Constant.business_account_status, object.getString(Constant.business_account_status));
                AppPreferences.setStringPreference(appContext, Constant.business_bio, object.getString(Constant.business_bio));
                AppPreferences.setStringPreference(appContext, Constant.business_logo, object.getString(Constant.business_logo));
                AppPreferences.setStringPreference(appContext, Constant.business_cover_image, object.getString(Constant.business_cover_image));
                AppPreferences.setStringPreference(appContext, Constant.business_country, object.getString(Constant.business_country));
                AppPreferences.setStringPreference(appContext, Constant.business_country_code, object.getString(Constant.business_country_code));
                AppPreferences.setStringPreference(appContext, Constant.business_city, object.getString(Constant.business_city));
                AppPreferences.setStringPreference(appContext, Constant.business_main_activity_id, object.getString(Constant.business_main_activity_id));
                AppPreferences.setStringPreference(appContext, Constant.business_sub_activity_id, object.getString(Constant.business_sub_activity_id));
                AppPreferences.setStringPreference(appContext, Constant.authentic_doc, object.getString(Constant.authentic_doc));
                AppPreferences.setStringPreference(appContext, Constant.doc_type, object.getString(Constant.doc_type));
                AppPreferences.setStringPreference(appContext, Constant.shelf_doc, object.getString(Constant.shelf_doc));
                AppPreferences.setStringPreference(appContext, Constant.business_creation_date, object.getString(Constant.business_creation_date));
                AppPreferences.setStringPreference(appContext, Constant.business_subscriptions_renew_date, object.getString(Constant.business_subscriptions_renew_date));
                AppPreferences.setStringPreference(appContext, Constant.business_exp_date, object.getString(Constant.business_exp_date));
                AppPreferences.setStringPreference(appContext, Constant.token_id, object.getString(Constant.token_id));
                AppPreferences.setStringPreference(appContext, Constant.created_at, object.getString(Constant.created_at));
                AppPreferences.setStringPreference(appContext, Constant.updated_at, object.getString(Constant.updated_at));
                startSession();
            } else {
                Alerts.showToast(appContext, "Invalid Email or Password");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startSession() {
        AppPreferences.setBooleanPreference(appContext, Constant.IS_LOGIN, true);
        AppPreferences.setBooleanPreference(appContext, Constant.IS_WITHOUT_LOGIN, false);
        Alerts.showToast(appContext, "Login success");

        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
        startActivity(new Intent(appContext, HomeActivity.class));
        finish();
    }

    private boolean isInputValid() {
        if (edt_email.getText().toString().trim().length() == 0) {
            Alerts.showToast(appContext, "Please enter name.");
            return false;
        } else if (!EmailChecker.checkEmail(edt_email.getText().toString().trim())) {
            Alerts.showToast(appContext, "Please enter valid email.");
            return false;
        } else if (edt_pass.getText().toString().trim().length() == 0) {
            Alerts.showToast(appContext, "Please enter password.");
            return false;
        } else if (!cd.isNetworkAvailable()) {
            cd.showAlert();
            return false;
        }
        return true;
    }
}
