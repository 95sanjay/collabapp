package com.info.collab.fragements;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.info.collab.R;
import com.info.collab.activity.ChatActivity;
import com.info.collab.activity.ProductInfo;
import com.info.collab.adapter.ProductListAdapter2;
import com.info.collab.modalClass.Product;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class CollabBusinessInfoFragment extends BaseFragment implements View.OnClickListener {

    private View view;
    TextView business_nameTV,mobileTV,bussiness_bio_tv;
    LinearLayout chat_layout;
    private TextView headingTV;
    private String imageurl;
    private String business_name,uni_id;

    public static CollabBusinessInfoFragment newInstance(Bundle args) {
        CollabBusinessInfoFragment fragment = new CollabBusinessInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.collabbusiness_info, container, false);
        init();
        return view;
    }

    void  init(){
        business_nameTV = view.findViewById(R.id.business_nameTV);
        mobileTV = view.findViewById(R.id.mobileTV);
        headingTV = view.findViewById(R.id.headingTV);
        bussiness_bio_tv = view.findViewById(R.id.bussiness_bio_tv);
        chat_layout = view.findViewById(R.id.chat_layout);
        chat_layout.setOnClickListener(this);
        productApi();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.chat_layout:
                Intent intent = new Intent(appContext, ChatActivity.class);
                intent.putExtra("user2_id",uni_id);
                intent.putExtra("imageurl",imageurl);
                intent.putExtra("business_name",business_name);
                startActivity(intent);
                break;
        }
    }

    private void productApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(getContext(), Constant.collab_uni_id ));

        RequestBody requestBody = builder.build();
        RetrofitService.Company_details(new Dialog(getContext()), retrofitApiClient.Company_details(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson(response);
            }

            @Override
            public void onResponseFailed(String error) {
            }
        });
    }

    private void parseJson(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject resObj = null;
            resObj = new JSONObject(response.body().string());
            String message = resObj.optString("message");
            String status = resObj.optString("status");
            JSONArray jsonArray = null;
            if (status.equals("true")) {
                jsonArray = resObj.getJSONArray("user_data");
                JSONObject object = null;
                for (int i = 0; i < jsonArray.length(); i++) {
                    object = jsonArray.getJSONObject(i);
                    business_nameTV.setText(object.getString("business_name"));
                    mobileTV.setText(object.getString("business_main_phone_number"));
                    bussiness_bio_tv.setText(object.getString("business_bio"));

                    imageurl = object.getString("business_logo");
                    business_name = object.getString("business_name");
                    uni_id = object.getString("uni_id");
                    Log.d("ChatActivity","user2_id     "+uni_id);
                    headingTV.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {

        }
    }
}