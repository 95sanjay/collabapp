package com.info.collab.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.info.collab.R;
import com.info.collab.adapter.CategoryAdapter;
import com.info.collab.adapter.SubActivityAdapter;
import com.info.collab.modalClass.CategoryMD;
import com.info.collab.modalClass.Product;
import com.info.collab.modalClass.SubCategoryMD;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.Alerts;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.BaseActivity;
import com.info.collab.utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class UpdateProductActivity extends BaseActivity implements View.OnClickListener{

    ImageView cancelBtn,backBtn;
    Spinner spin1,spin_categoury,spin_sub_categoury;
    EditText desc_tv,edt2;
    ImageView add_img;
    TextView btn_add;
    public final int REQUEST_CODE_PICKER = 125;
    ArrayList<Image> images = new ArrayList<>();
    String[] AgeArrayList1 = {"Reseller","seller"};
    String stt1;
    byte[] imageBytes;
    String timeStamp="";
    String extension="";
    private String categoury_selected,sub_categoury_selected;
    Dialog dialog;
    TextView title;
    Product product;
    private ArrayList<CategoryMD> categoryMDArrayList;
    private ArrayList<CategoryMD> categoryMDArrayList2 = new ArrayList<>();
    private ArrayList<SubCategoryMD> subCategoryMDArrayList;
    private ArrayList<SubCategoryMD> subCategoryMDArrayList2 = new ArrayList<>();
    private String main_activity,sub_activity;

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);
        setStatusBarGradiant(this);
        init();
    }

    void init(){
        dialog = new Dialog(this);
        spin1 = findViewById(R.id.spin1);
        backBtn = findViewById(R.id.backBtn);
        desc_tv = findViewById(R.id.desc_tv);
        add_img = findViewById(R.id.add_img);
        btn_add = findViewById(R.id.btn_add);
        cancelBtn = findViewById(R.id.cancelBtn);
        edt2 = findViewById(R.id.edt2);
        spin_categoury = findViewById(R.id.spin_categoury);
        spin_sub_categoury = findViewById(R.id.spin_sub_categoury);
        title = findViewById(R.id.title);
        title.setText("Update Product");

        add_img.setOnClickListener(this);
        btn_add.setOnClickListener(this);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ArrayAdapter aa1 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, AgeArrayList1);
        aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin1.setAdapter(aa1);
        spin1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                stt1 = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Intent i = getIntent();
        product = (Product) i.getSerializableExtra("product");
        if (i != null){
            btn_add.setText("Update Product");
            edt2.setText(product.getEnglish_name());
          //  edt3.setText(product.getProduct_category_id());
          //  edt4.setText(product.getProduct_sub_category_id());
            desc_tv.setText(product.getProduct_description());
            Glide.with(add_img.getContext()).load(product.getProduct_main_image()).into(add_img);
        }

        categouryApi();
    }


    private void categouryApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", "1");
        RequestBody requestBody = builder.build();
        RetrofitService.Company_category_list(new Dialog(UpdateProductActivity.this), retrofitApiClient.Company_category_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("data");
                        JSONObject object = null;
                        categoryMDArrayList = new ArrayList<>();
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            object = jsonArray.getJSONObject(i);
                            CategoryMD categoryMD = new CategoryMD();
                            categoryMD.setId(object.getString("id"));
                            categoryMD.setName(object.getString("name"));
                            categoryMDArrayList.add(categoryMD);
                        }
                        CategoryMD categoryMD = new CategoryMD();
                        categoryMD.setId("0");
                        categoryMD.setName("Select");
                        categoryMDArrayList2.add(categoryMD);
                        categoryMDArrayList2.addAll(categoryMDArrayList);
                        CategoryAdapter categoryAdapter = new CategoryAdapter(getApplicationContext(),categoryMDArrayList2);
                        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spin_categoury.setAdapter(categoryAdapter);
                        spin_categoury.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                main_activity = categoryMDArrayList2.get(i).getId();
                                if (main_activity.equalsIgnoreCase("0")){
                                    subCategoryMDArrayList2.clear();
                                    SubCategoryMD subCategoryMD = new SubCategoryMD();
                                    subCategoryMD.setId("0");
                                    subCategoryMD.setName("Select");
                                    subCategoryMDArrayList2.add(subCategoryMD);
                                    SubActivityAdapter subActivityAdapter = new SubActivityAdapter(getApplicationContext(),subCategoryMDArrayList2);
                                    subActivityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spin_sub_categoury.setAdapter(subActivityAdapter);
                                }else {
                                    subcategouryApi(main_activity);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                }catch (Exception e){}
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void subcategouryApi(String id){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("category_id", id);
        RequestBody requestBody = builder.build();
        RetrofitService.Company_subcategory_list(new Dialog(UpdateProductActivity.this), retrofitApiClient.Company_subcategory_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("subcategory_list");
                        JSONObject object = null;
                        subCategoryMDArrayList = new ArrayList<>();
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            object = jsonArray.getJSONObject(i);
                            SubCategoryMD subCategoryMD = new SubCategoryMD();
                            subCategoryMD.setId(object.getString("id"));
                            subCategoryMD.setName(object.getString("name"));
                            subCategoryMDArrayList.add(subCategoryMD);
                        }
                        SubActivityAdapter subActivityAdapter = new SubActivityAdapter(getApplicationContext(),subCategoryMDArrayList);
                        subActivityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spin_sub_categoury.setAdapter(subActivityAdapter);
                        spin_sub_categoury.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                sub_activity = subCategoryMDArrayList.get(i).getId();
                                subCategoryMDArrayList2.clear();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                }catch (Exception e){}
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.add_img:
                selectimg();
                break;

            case R.id.btn_add:
                if (isInputValid()){
                    AddProductApi();
                }

                break;
        }
    }

    private boolean isInputValid() {
        if (edt2.getText().toString().trim().length() == 0) {
            Alerts.showToast(getApplicationContext(), "Please enter name.");
            return false;
        } else if (main_activity.equalsIgnoreCase("Select")) {
            Alerts.showToast(getApplicationContext(), "Please enter category.");
            return false;
        } else if (sub_activity.equalsIgnoreCase("Select")) {
            Alerts.showToast(getApplicationContext(), "Please enter sub category.");
            return false;
        }/* else if (images.size() == 0) {
            Alerts.showToast(getApplicationContext(), "Please select image.");
            return false;
        }*/
        return true;
    }

    private void AddProductApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(getApplicationContext(), Constant.uni_id ))
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(getApplicationContext(), Constant.token_id ))
                .addFormDataPart("english_name", edt2.getText().toString().trim())
                .addFormDataPart("category", main_activity)
                .addFormDataPart("description", desc_tv.getText().toString().trim())
                .addFormDataPart("seller_type", stt1)
                .addFormDataPart("product_id", product.getProduct_identifier())
                .addFormDataPart("arabic_name", edt2.getText().toString().trim())
                .addFormDataPart("sub_category", sub_activity);

        if (images.size() !=0 ) {
            if (images.get(0).getPath() != null) {
                File featured_image = new File(images.get(0).getPath());
                if (featured_image.exists()) {
                    builder.addFormDataPart("product_mainimg", featured_image.getName(), RequestBody.create(MultipartBody.FORM, featured_image));
                }
            }
        }

        RequestBody requestBody = builder.build();
        RetrofitService.Update_product(new Dialog(UpdateProductActivity.this), retrofitApiClient.Update_product(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson(response);
            }

            @Override
            public void onResponseFailed(String error) {
            }
        });
    }

    private void parseJson(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject jsonObject = new JSONObject(response.body().string());
            if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                Toast.makeText(this,jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                setResult(222, intent);
                finish();
            }
        } catch (Exception e) {

        }
    }


    void selectimg() {
        ImagePicker.create(UpdateProductActivity.this)
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .theme(R.style.ImagePickerTheme)
                .single()// max images can be selected (99 by default)
                .showCamera(false) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image ("Camera" folder by default)
                .origin(images) // original selected images, used in multi mode
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }


    @Override
    public void onActivityResult(int requestCode, final int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICKER && resultCode == Activity.RESULT_OK && data != null) {
            images.clear();
            images.addAll(ImagePicker.getImages(data));
            if (images.size() > 0) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeFile(images.get(0).getPath());
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                imageBytes = baos.toByteArray();
                String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                Long tsLong = System.currentTimeMillis() / 1000;
                timeStamp = tsLong.toString();
                String filenameArray[] = images.get(0).getName().split("\\.");
                extension = filenameArray[filenameArray.length - 1];

                Log.d("image","}}}}}}}}}}===  "+imageBytes+"\n"+timeStamp+"\n"+extension);
                add_img.setImageBitmap(bitmap);
            }
        }
    }

    public void successdialog(String message){
        final Dialog dialog = new Dialog(UpdateProductActivity.this);
        dialog.setContentView(R.layout.error_dialog);
        TextView txt = dialog.findViewById(R.id.messagetv);
        txt.setText(message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        Button declineButton = (Button) dialog.findViewById(R.id.btnDialogCancel);
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button videoButton = (Button) dialog.findViewById(R.id.btnDialogOk);
        videoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void errordialog(String message){
        final Dialog dialog = new Dialog(UpdateProductActivity.this);
        dialog.setContentView(R.layout.sucess_dialog);
        TextView txt = dialog.findViewById(R.id.messagetv);
        txt.setText(message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        Button declineButton = (Button) dialog.findViewById(R.id.btnDialogCancel);
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button videoButton = (Button) dialog.findViewById(R.id.btnDialogOk);
        videoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
}