package com.info.collab.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.info.collab.R;
import com.info.collab.modalClass.ServiceMD;

import java.util.ArrayList;

public class ServiceListAdapter2 extends RecyclerView.Adapter<ServiceListAdapter2.ImageViewHolder> {
    ArrayList<ServiceMD> imageArrayList;
    private final OnServiceClick listener;

    public interface OnItemserviceList {
        void onItemClick(ServiceMD item);
    }

    public ServiceListAdapter2(ArrayList<ServiceMD> imageArrayList, OnServiceClick listener) {
        this.imageArrayList = imageArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ImageViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pro_type3, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int i) {
        ServiceMD product = imageArrayList.get(i);

        imageViewHolder.text_p_type.setText(product.getEnglish_name());
        Glide.with(imageViewHolder.IType.getContext()).load(product.getService_main_image()).into(imageViewHolder.IType);

        imageViewHolder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            //    listener.onItemClick(product);
            }
        });

        imageViewHolder.delete_btn.setVisibility(View.GONE);
        imageViewHolder.edit_btn.setVisibility(View.GONE);

        imageViewHolder.delete_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onServiceDelete(product);
            }
        });

        imageViewHolder.edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onServiceEdit(product);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageArrayList.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder  {
        ImageView IType,edit_btn,delete_btn;
        TextView text_p_type;
        LinearLayout item;

        public ImageViewHolder(View itemView) {
            super(itemView);
            IType = itemView.findViewById(R.id.IType);
            item = itemView.findViewById(R.id.item);
            delete_btn = itemView.findViewById(R.id.delete_btn);
            text_p_type = itemView.findViewById(R.id.text_p_type);
            edit_btn = itemView.findViewById(R.id.edit_btn);
        }
    }

    public interface OnServiceClick {
        void onServiceDelete(ServiceMD item);
        void onServiceEdit(ServiceMD item);
    }
}
