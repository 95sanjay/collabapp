package com.info.collab.utils;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.info.collab.retrofits_provider.ApiClient;
import com.info.collab.retrofits_provider.RetrofitService;


public class BaseActivity extends AppCompatActivity {

    public ApiClient retrofitApiClient;
    public ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        retrofitApiClient = RetrofitService.getRetrofit();
        cd = new ConnectionDetector(this);
    }
}
