package com.info.collab.retrofits_provider;

import com.info.collab.utils.Constant;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiClient {
    @POST(Constant.SIGN_UP)
    Call<ResponseBody> singUp(@Body RequestBody body);

    @POST(Constant.SIGN_IN)
    Call<ResponseBody> signIn(@Body RequestBody body);

    @POST(Constant.registration)
    Call<ResponseBody> Registration(@Body RequestBody file);

    @POST(Constant.edit_user)
    Call<ResponseBody> Edit_user(@Body RequestBody file);

    @POST(Constant.getNewInProduct)
    Call<ResponseBody> NewInProduct_list(@Body RequestBody file);

    @POST(Constant.categorylist)
    Call<ResponseBody> Category_list(@Body RequestBody file);

    @POST(Constant.get_company_list)
    Call<ResponseBody> Collab_list(@Body RequestBody file);

    @POST(Constant.filter)
    Call<ResponseBody> Filter(@Body RequestBody file);

    @POST(Constant.live_feed)
    Call<ResponseBody> Live_feed(@Body RequestBody file);

    @POST(Constant.get_company_category_list)
    Call<ResponseBody> Company_category_list(@Body RequestBody file);

    @POST(Constant.get_userchat_list)
    Call<ResponseBody> Userchat_list(@Body RequestBody file);

    @POST(Constant.post)
    Call<ResponseBody> feed_post(@Body RequestBody file);

    @POST(Constant.add_service)
    Call<ResponseBody> add_service(@Body RequestBody file);

    @POST(Constant.edit_service)
    Call<ResponseBody> Edit_service(@Body RequestBody file);

    @POST(Constant.add_product)
    Call<ResponseBody> addProduct(@Body RequestBody file);

    @POST(Constant.edit_product)
    Call<ResponseBody> Update_product(@Body RequestBody file);

    @POST(Constant.get_product_list)
    Call<ResponseBody> Product_list(@Body RequestBody file);

    @POST(Constant.get_company_details)
    Call<ResponseBody> Company_details(@Body RequestBody file);

    @POST(Constant.get_company_subcategory_list)
    Call<ResponseBody> Company_subcategory_list(@Body RequestBody file);

    @POST(Constant.get_service_list)
    Call<ResponseBody> Service_list(@Body RequestBody file);

    @POST(Constant.delete_product)
    Call<ResponseBody> delete_product(@Body RequestBody file);

    @POST(Constant.delete_service)
    Call<ResponseBody> Delete_service(@Body RequestBody file);

    @POST(Constant.logout_user)
    Call<ResponseBody> Logout_user(@Body RequestBody file);

    @POST(Constant.product_info)
    Call<ResponseBody> Product_info(@Body RequestBody file);

    @POST(Constant.get_chat_list)
    Call<ResponseBody> Chat_list(@Body RequestBody file);

    @POST(Constant.chating)
    Call<ResponseBody> Chating(@Body RequestBody file);

    @POST(Constant.delete_live_feed)
    Call<ResponseBody> Delete_live_feed(@Body RequestBody file);

    @POST(Constant.post_comment)
    Call<ResponseBody> Post_comment(@Body RequestBody file);

    @POST(Constant.get_category_products)
    Call<ResponseBody> Category_products(@Body RequestBody file);

    @POST(Constant.post_like)
    Call<ResponseBody> Post_like(@Body RequestBody file);

    @POST(Constant.follow)
    Call<ResponseBody> Follow(@Body RequestBody file);

    @POST(Constant.get_state_list)
    Call<ResponseBody> State_list(@Body RequestBody file);

    @POST(Constant.get_city_list)
    Call<ResponseBody> City_list(@Body RequestBody file);

    @POST(Constant.business_list)
    Call<ResponseBody> Business_list(@Body RequestBody file);

    @POST(Constant.get_subcategory_products)
    Call<ResponseBody> Subcategory_products(@Body RequestBody file);

}
