package com.info.collab.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.info.collab.R;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.BaseActivity;
import com.info.collab.utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class ProductInfo extends BaseActivity implements View.OnClickListener{

    ImageView cancelBtn,backBtn,product_img;
    private String product_id;
    private ImageView chat_btn;
    TextView discripitionTV,categoryTV,product_nameTV;
    private String user2_id;
    private TextView tt1,tt2;
    private String imageurl;
    private String business_name;

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_info);
        setStatusBarGradiant(this);
        product_id = getIntent().getExtras().getString("product_id");
        backBtn = findViewById(R.id.backBtn);
        chat_btn = findViewById(R.id.chat_btn);
        backBtn.setOnClickListener(view -> onBackPressed());
        init();
    }

    void init(){
        product_img = findViewById(R.id.product_img);
        discripitionTV = findViewById(R.id.discripitionTV);
        categoryTV = findViewById(R.id.categoryTV);
        product_nameTV = findViewById(R.id.product_nameTV);
        tt1 = findViewById(R.id.tt1);
        tt2 = findViewById(R.id.tt2);

        productApi();
    }

    @Override
    public void onClick(View view) { }

    private void productApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(ProductInfo.this, Constant.uni_id ))
                .addFormDataPart("product_id", product_id)
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(ProductInfo.this, Constant.token_id ));
        RequestBody requestBody = builder.build();
        RetrofitService.Product_info(new Dialog(ProductInfo.this), retrofitApiClient.Product_info(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson(response);
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void parseJson(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject resObj = null;
            resObj = new JSONObject(response.body().string());
            String message = resObj.optString("message");
            String status = resObj.optString("status");

            if (status.equals("true")) {
                JSONObject object = resObj.getJSONObject("products");
                String name = object.getString("s_english");
                String product_description = object.getString("product_description");
                String product_main_image = object.getString("product_main_image");

               JSONArray jsonArray = object.getJSONArray("user");
               for (int j = 0 ; j < jsonArray.length() ; j++){
                   JSONObject object1 =  jsonArray.getJSONObject(j);
                   tt1.setText(object1.getString("business_address"));
                   tt2.setText(object1.getString("business_name"));
                   user2_id = object1.getString("uni_id");
                   imageurl = object1.getString("business_logo");
                   business_name = object1.getString("business_name");
               }
                product_nameTV.setText(name);
                categoryTV.setText("Electronics");
                discripitionTV.setText(product_description);

                Glide.with(product_img)
                        .asBitmap()
                        .load(product_main_image)
                        .error(R.drawable.demo)
                        .into(product_img);

                if (user2_id.equalsIgnoreCase(AppPreferences.getStringPreference(ProductInfo.this, Constant.uni_id )))
                {
                    chat_btn.setVisibility(View.GONE);
                }else {
                    chat_btn.setOnClickListener(view -> {
                        Intent intent = new Intent(this,ChatActivity.class);
                        intent.putExtra("user2_id",user2_id);
                        intent.putExtra("imageurl",imageurl);
                        intent.putExtra("business_name",business_name);
                        startActivity(intent);
                    });
                }

            }else {
                Toast.makeText(ProductInfo.this, message, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}