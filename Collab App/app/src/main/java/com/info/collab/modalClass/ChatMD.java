package com.info.collab.modalClass;

public class ChatMD {
    private String id;
    private String user1_id;
    private String user2_id;
    private String message;
    private String created_at;
    private String updated_at;
    private String business_name;
    private String business_logo;
    private String business_owner;
    private String business_owner2;
    private String business_logo2;

    public String getBusiness_owner2() {
        return business_owner2;
    }

    public void setBusiness_owner2(String business_owner2) {
        this.business_owner2 = business_owner2;
    }

    public String getBusiness_logo2() {
        return business_logo2;
    }

    public void setBusiness_logo2(String business_logo2) {
        this.business_logo2 = business_logo2;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getBusiness_logo() {
        return business_logo;
    }

    public void setBusiness_logo(String business_logo) {
        this.business_logo = business_logo;
    }

    public String getBusiness_owner() {
        return business_owner;
    }

    public void setBusiness_owner(String business_owner) {
        this.business_owner = business_owner;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser1_id() {
        return user1_id;
    }

    public void setUser1_id(String user1_id) {
        this.user1_id = user1_id;
    }

    public String getUser2_id() {
        return user2_id;
    }

    public void setUser2_id(String user2_id) {
        this.user2_id = user2_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}