package com.info.collab.fragements;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.info.collab.R;
import com.info.collab.activity.ProductInfo;
import com.info.collab.adapter.ProductListAdapter2;
import com.info.collab.modalClass.Product;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class ProductInfoFragment extends BaseFragment implements View.OnClickListener {

    private View view;
    RecyclerView product_list_rv;
    private ArrayList<Product> productslist;
    private ProductListAdapter2 productAdapter;

    public static ProductInfoFragment newInstance(Bundle args) {
        ProductInfoFragment fragment = new ProductInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_product_info, container, false);
        init();
        return view;
    }

    void  init(){
        product_list_rv = view.findViewById(R.id.product_list_rv);
        productApi();
    }

    @Override
    public void onClick(View view) {

    }

    private void productApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(getContext(), Constant.uni_id ))
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(getContext(), Constant.token_id ));

        RequestBody requestBody = builder.build();
        RetrofitService.Product_List(new Dialog(getContext()), retrofitApiClient.Product_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson(response);
            }

            @Override
            public void onResponseFailed(String error) {
            }
        });
    }


    private void parseJson(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject resObj = null;
            resObj = new JSONObject(response.body().string());
            String message = resObj.optString("message");
            String status = resObj.optString("status");
            JSONArray jsonArray = null;
            if (status.equals("true")) {
                jsonArray = resObj.getJSONArray("product_list");
                JSONObject object = null;
                productslist = new ArrayList<>();
                for (int i = 0 ; i < jsonArray.length() ; i++){
                    object = jsonArray.getJSONObject(i);
                    Product product = new Product();
                    product.setProduct_identifier(object.getString("product_identifier"));
                    product.setBID(object.getString("BID"));
                    product.setHs_code(object.getString("hs_code"));
                    product.setSeller_type(object.getString("seller_type"));
                    product.setArabic_name(object.getString("arabic_name"));
                    product.setEnglish_name(object.getString("english_name"));
                    product.setProduct_category_id(object.getString("product_category_id"));
                    product.setProduct_sub_category_id(object.getString("product_sub_category_id"));
                    product.setProduct_description(object.getString("product_description"));
                    product.setProduct_main_image(object.getString("product_main_image"));
                    product.setProduct_creation_date(object.getString("product_creation_date"));
                    productslist.add(product);
                }
                productAdapter = new ProductListAdapter2(productslist, new ProductListAdapter2.OnProductClick() {
                    @Override
                    public void onProductDelete(Product item) {
                    }

                    @Override
                    public void onProductEdit(Product item) {
                        Intent intent = new Intent(getContext(), ProductInfo.class);
                        intent.putExtra("product_id",item.getProduct_identifier());
                        startActivity(intent);
                    }
                });
                product_list_rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                product_list_rv.setItemAnimator(new DefaultItemAnimator());
                product_list_rv.setAdapter(productAdapter);
                productAdapter.notifyDataSetChanged();
            }else {
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {

        }
    }
}