package com.info.collab.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.info.collab.R;
import com.info.collab.modalClass.CommentMd;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentAdp extends RecyclerView.Adapter<CommentAdp.ImageViewHolder> {
    ArrayList<CommentMd> imageArrayList;

    public CommentAdp(ArrayList<CommentMd> imageArrayList) {
        this.imageArrayList = imageArrayList;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ImageViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comment_row, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder v, int i) {
        CommentMd comment = imageArrayList.get(i);
        v.reciveTV.setText(comment.getComment());
        v.nameTV.setText(comment.getBusiness_name());
        v.timeTV.setText(comment.getTime());
        Glide.with(v.userImg.getContext()).load(comment.getBusiness_logo()).error(R.drawable.test_logo).into(v.userImg);
    }

    @Override
    public int getItemCount() {
        return imageArrayList.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder  {
        TextView reciveTV,nameTV,timeTV;
        CircleImageView userImg;

        public ImageViewHolder(View itemView) {
            super(itemView);
            nameTV = itemView.findViewById(R.id.nameTV);
            reciveTV = itemView.findViewById(R.id.reciveTV);
            userImg = itemView.findViewById(R.id.userImg);
            timeTV = itemView.findViewById(R.id.timeTV);
        }
    }
}
