package com.info.collab.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.info.collab.R;
import com.info.collab.modalClass.Product;

import java.util.ArrayList;

public class ProductListAdapter2 extends RecyclerView.Adapter<ProductListAdapter2.ImageViewHolder> {
    ArrayList<Product> imageArrayList;
    private final OnProductClick listener;

    public ProductListAdapter2(ArrayList<Product> imageArrayList, OnProductClick listener) {
        this.imageArrayList = imageArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ImageViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pro_type3, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int i) {
        Product product = imageArrayList.get(i);

        imageViewHolder.text_p_type.setText(product.getEnglish_name());
        Glide.with(imageViewHolder.IType.getContext()).load(product.getProduct_main_image()).into(imageViewHolder.IType);
        imageViewHolder.delete_btn.setVisibility(View.GONE);
        imageViewHolder.edit_btn.setVisibility(View.GONE);

       /* imageViewHolder.delete_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onProductDelete(product);
            }
        });

        imageViewHolder.edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onProductEdit(product);
            }
        });*/

        imageViewHolder.next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onProductEdit(product);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageArrayList.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder  {
        ImageView IType,delete_btn,edit_btn,next_btn;
        TextView text_p_type;
        LinearLayout item;

        public ImageViewHolder(View itemView) {
            super(itemView);
            IType = itemView.findViewById(R.id.IType);
            item = itemView.findViewById(R.id.item);
            delete_btn = itemView.findViewById(R.id.delete_btn);
            edit_btn = itemView.findViewById(R.id.edit_btn);
            next_btn = itemView.findViewById(R.id.next_btn);
            text_p_type = itemView.findViewById(R.id.text_p_type);
        }
    }

    public interface OnProductClick {
        void onProductDelete(Product item);
        void onProductEdit(Product item);
    }
}
