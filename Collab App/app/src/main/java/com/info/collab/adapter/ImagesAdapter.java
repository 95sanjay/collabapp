package com.info.collab.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.model.Image;
import com.info.collab.R;

import java.util.ArrayList;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ImageViewHolder> {
    ArrayList<Image> imageArrayList;

    public ImagesAdapter(ArrayList<Image> imageArrayList) {
        this.imageArrayList = imageArrayList;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ImageViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_img, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int i) {
        Glide.with(imageViewHolder.selected_img.getContext()).load(imageArrayList.get(i).getPath()).into(imageViewHolder.selected_img);
    }

    @Override
    public int getItemCount() {
        return imageArrayList.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder  {
        ImageView selected_img;

        public ImageViewHolder(View itemView) {
            super(itemView);
            selected_img = itemView.findViewById(R.id.selected_img);

        }


    }





}
