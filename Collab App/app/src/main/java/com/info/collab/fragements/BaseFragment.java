package com.info.collab.fragements;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import com.info.collab.retrofits_provider.ApiClient;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.utils.ConnectionDetector;

public abstract class BaseFragment extends Fragment {
    public ApiClient retrofitApiClient;
    public ConnectionDetector cd;
    public Context appContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appContext = getActivity();
        retrofitApiClient = RetrofitService.getRetrofit();
        cd = new ConnectionDetector(appContext);
    }
}
