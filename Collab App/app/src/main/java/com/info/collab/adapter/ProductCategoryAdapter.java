package com.info.collab.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.info.collab.R;
import com.info.collab.modalClass.Product;
import com.info.collab.modalClass.ProductCategory;
import com.info.collab.utils.OnItemCategoryClick;

import java.util.ArrayList;

public class ProductCategoryAdapter extends RecyclerView.Adapter<ProductCategoryAdapter.ImageViewHolder> {
    ArrayList<ProductCategory> imageArrayList;
    OnItemCategoryClick listener;

    public ProductCategoryAdapter(ArrayList<ProductCategory> imageArrayList,OnItemCategoryClick listener) {
        this.imageArrayList = imageArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ImageViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pro_type, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int i) {
        ProductCategory product = imageArrayList.get(i);
        imageViewHolder.text_p_type.setText(product.getCategory_english_name());
        Glide.with(imageViewHolder.IType.getContext()).load(product.getCategory_main_image()).into(imageViewHolder.IType);
        imageViewHolder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(product);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageArrayList.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder  {
        ImageView IType;
        TextView text_p_type;
        LinearLayout item;

        public ImageViewHolder(View itemView) {
            super(itemView);
            IType = itemView.findViewById(R.id.IType);
            text_p_type = itemView.findViewById(R.id.text_p_type);
            item = itemView.findViewById(R.id.item);
        }
    }
}
