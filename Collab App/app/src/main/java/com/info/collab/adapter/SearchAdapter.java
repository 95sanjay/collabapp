package com.info.collab.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.info.collab.R;
import com.info.collab.modalClass.Product;

import java.util.ArrayList;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ImageViewHolder> implements Filterable {
    ArrayList<Product> imageArrayList;
    ArrayList<Product> imageArrayListFilterable;
    private final OnItemNewInClick listener;

    public SearchAdapter(ArrayList<Product> imageArrayList, OnItemNewInClick listener) {
        this.imageArrayList = imageArrayList;
        this.imageArrayListFilterable = imageArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ImageViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_search, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int i) {
        Product product = imageArrayListFilterable.get(i);

        imageViewHolder.text_p_type.setText(product.getEnglish_name());
        Glide.with(imageViewHolder.IType.getContext()).load(product.getProduct_main_image()).into(imageViewHolder.IType);

        imageViewHolder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(product);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageArrayListFilterable.size();
    }


    public class ImageViewHolder extends RecyclerView.ViewHolder  {
        ImageView IType;
        TextView text_p_type;
        LinearLayout item;

        public ImageViewHolder(View itemView) {
            super(itemView);
            IType = itemView.findViewById(R.id.IType);
            item = itemView.findViewById(R.id.item);
            text_p_type = itemView.findViewById(R.id.text_p_type);
        }
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    imageArrayListFilterable = imageArrayList;
                } else {
                    ArrayList<Product> filteredList = new ArrayList<>();
                    for (Product row : imageArrayList) {
                        if (row.getEnglish_name().toLowerCase().contains(charString.toLowerCase()) || row.getBusiness_address().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    imageArrayListFilterable = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = imageArrayListFilterable;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                imageArrayListFilterable = (ArrayList<Product>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
    public interface OnItemNewInClick {
        void onItemClick(Product item);
    }
}
