package com.info.collab.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.info.collab.R;
import com.info.collab.adapter.ChatingAdapter;
import com.info.collab.modalClass.ChatMD;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.BaseActivity;
import com.info.collab.utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class ChatActivity extends BaseActivity {

    RecyclerView chating_rv;
    LinearLayout send_btn;
    EditText content_edt;
    private ArrayList<ChatMD> chatArrayList = new ArrayList<>();
    private ChatingAdapter chatingAdapter;
    private String user2_id;
    ImageView backBtn;
    CircleImageView profile_img;
    TextView day_txt;
    Handler handler ;
    private Runnable runnableCode;
    private String imageurl;
    private String business_name;
    private TextView title;
    private int listSize = 0;

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        setContentView(R.layout.activity_chat);
        user2_id = getIntent().getStringExtra("user2_id");
        imageurl = getIntent().getStringExtra("imageurl");
        business_name = getIntent().getStringExtra("business_name");

        Log.d("ChatActivity","user2_id     "+user2_id);
        Log.d("ChatActivity","user1_id     "+AppPreferences.getStringPreference(ChatActivity.this, Constant.uni_id));

        init();
    }

    void  init(){
        chating_rv = findViewById(R.id.chating_rv);
        content_edt = findViewById(R.id.content_edt);
        backBtn = findViewById(R.id.backBtn);
        day_txt = findViewById(R.id.day_txt);
        title = findViewById(R.id.title);
        send_btn = findViewById(R.id.send_btn);
        profile_img = findViewById(R.id.profile_img);
        title.setText(business_name);
        Glide.with(profile_img.getContext()).load(imageurl).into(profile_img);
        send_btn.setOnClickListener(view -> ChatingApi());
        backBtn.setOnClickListener(view -> onBackPressed());

        handler = new Handler();
        runnableCode = new Runnable() {
            @Override
            public void run() {
                //Log.d("Handlers", "Called on main thread");
                Chat_list_Api();
                 handler.postDelayed(this, 1000);
            }
        };
        handler.post(runnableCode);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
         handler.removeCallbacks(runnableCode);
    }

    private void Chat_list_Api(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(ChatActivity.this, Constant.uni_id ))
                .addFormDataPart("user2_id",user2_id)
                .addFormDataPart("user1_id",AppPreferences.getStringPreference(ChatActivity.this, Constant.uni_id ));
        RequestBody requestBody = builder.build();
        RetrofitService.Chat_list(new Dialog(ChatActivity.this), retrofitApiClient.Chat_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson(response);
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void parseJson(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject resObj = null;
            resObj = new JSONObject(response.body().string());
           // Log.e("result", "" + resObj);
            String message = resObj.optString("message");
            String status = resObj.optString("status");
            JSONArray jsonArray = null;
            if (status.equals("true")) {
                jsonArray = resObj.getJSONArray("data");
                JSONObject object = null;
                chatArrayList = new ArrayList<>();
                for (int i = 0 ; i < jsonArray.length() ; i++){
                    object = jsonArray.getJSONObject(i);
                    ChatMD product = new ChatMD();
                    product.setId(object.getString("id"));
                    product.setUser1_id(object.getString("user1_id"));
                    product.setUser2_id(object.getString("user2_id"));
                    product.setMessage(object.getString("message"));
                    product.setCreated_at(object.getString("created_at"));
                    product.setUpdated_at(object.getString("updated_at"));
                    product.setBusiness_name(object.getString("business_name"));
                    product.setBusiness_logo(object.getString("business_logo"));
                    product.setBusiness_owner(object.getString("business_owner"));
                    chatArrayList.add(product);
                }

                if (listSize != chatArrayList.size()){
                    chatingAdapter = new ChatingAdapter(ChatActivity.this,chatArrayList);
                    chating_rv.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                    chating_rv.setItemAnimator(new DefaultItemAnimator());
                    chating_rv.setAdapter(chatingAdapter);
                    chating_rv.smoothScrollToPosition(chatingAdapter.getItemCount() - 1);
                    day_txt.setText("Today");
                    listSize = chatArrayList.size();
                }

            }else {
                day_txt.setText(message);
               //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ChatingApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(ChatActivity.this, Constant.uni_id ))
                .addFormDataPart("user2_id",user2_id)
                .addFormDataPart("chat",content_edt.getText().toString().trim())
                .addFormDataPart("user1_id",AppPreferences.getStringPreference(ChatActivity.this, Constant.uni_id ))
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(ChatActivity.this, Constant.token_id ));
        RequestBody requestBody = builder.build();
        RetrofitService.Chating(new Dialog(ChatActivity.this), retrofitApiClient.Chating(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    if (status.equals("true")) {
                        content_edt.setText("");
                        Chat_list_Api();
                    }else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }
}