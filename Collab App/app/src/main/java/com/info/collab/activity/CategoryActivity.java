package com.info.collab.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.info.collab.R;
import com.info.collab.adapter.SearchAdapter;
import com.info.collab.modalClass.Product;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.BaseActivity;
import com.info.collab.utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class CategoryActivity extends BaseActivity implements View.OnClickListener{

    RecyclerView feed_rv;
    private ArrayList<Product> productslist;
    private SearchAdapter productAdapter;
    ImageView addBtn,backBtn;
    SearchView edt_search;
    private String category_id;
    TextView title;
    CardView search_cv;
    CardView filter_card;

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_list2);
        category_id = getIntent().getExtras().getString("category_id");
        setStatusBarGradiant(this);
        init();
    }

    void init(){
        feed_rv = findViewById(R.id.feed_rv);
        backBtn = findViewById(R.id.backBtn);
        addBtn = findViewById(R.id.addBtn);
        title = findViewById(R.id.title);
        filter_card = findViewById(R.id.filter_card);
        filter_card.setVisibility(View.GONE);
        edt_search = findViewById(R.id.edt_search);
        search_cv = findViewById(R.id.search_cv);
        title.setText("Collab");
        backBtn.setOnClickListener(this);
        addBtn.setOnClickListener(view -> {
            Intent intent = new Intent(CategoryActivity.this, AddProductActivity.class);
            startActivity(intent);
        });
        productApi();

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        edt_search.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        edt_search.setMaxWidth(Integer.MAX_VALUE);
        edt_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                productAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                productAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.backBtn:
                onBackPressed();
                break;
        }
    }

    private void productApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(CategoryActivity.this, Constant.uni_id ))
                .addFormDataPart("category", category_id)
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(CategoryActivity.this, Constant.token_id ));

        RequestBody requestBody = builder.build();
        RetrofitService.Category_products(new Dialog(CategoryActivity.this), retrofitApiClient.Category_products(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("data");
                        JSONObject object = null;
                        productslist = new ArrayList<>();
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            object = jsonArray.getJSONObject(i);
                            Product product = new Product();
                            product.setProduct_identifier(object.getString("product_identifier"));
                            product.setBID(object.getString("BID"));
                            product.setHs_code(object.getString("hs_code"));
                            product.setSeller_type(object.getString("seller_type"));
                            product.setArabic_name(object.getString("arabic_name"));
                            product.setEnglish_name(object.getString("english_name"));
                            product.setProduct_category_id(object.getString("product_category_id"));
                            product.setProduct_sub_category_id(object.getString("product_sub_category_id"));
                            product.setProduct_description(object.getString("product_description"));
                            product.setProduct_main_image(object.getString("product_main_image"));
                            product.setProduct_creation_date(object.getString("product_creation_date"));
                            productslist.add(product);
                        }
                        productAdapter = new SearchAdapter(productslist, new SearchAdapter.OnItemNewInClick() {
                            @Override
                            public void onItemClick(Product item) {
                                Intent intent = new Intent(getApplicationContext(), ProductInfo.class);
                                intent.putExtra("product_id",item.getProduct_identifier());
                                startActivity(intent);
                            }
                        });
                        feed_rv.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                        feed_rv.setItemAnimator(new DefaultItemAnimator());
                        feed_rv.setAdapter(productAdapter);
                        productAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }
}