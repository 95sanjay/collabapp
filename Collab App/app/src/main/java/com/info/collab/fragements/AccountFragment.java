package com.info.collab.fragements;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.info.collab.R;
import com.info.collab.activity.ProductListActivity;
import com.info.collab.activity.ProfileActivity;
import com.info.collab.activity.ServiceListActivity;
import com.info.collab.activity.SignInActivity;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.Constant;

import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class AccountFragment extends BaseFragment implements View.OnClickListener {

    private View view;
    LinearLayout signout_ll,add_product_l,profile_layout,service_list;
    ToggleButton switch_show_protected;

    public static AccountFragment newInstance(Bundle args) {
        AccountFragment fragment = new AccountFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_account, container, false);
        init();
        return view;
    }

    void  init(){
        signout_ll = view.findViewById(R.id.signout_ll);
        add_product_l = view.findViewById(R.id.add_product_l);
        profile_layout = view.findViewById(R.id.profile_layout);
        service_list = view.findViewById(R.id.service_list);
        switch_show_protected = view.findViewById(R.id.switch_show_protected);

        signout_ll.setOnClickListener(this);
        add_product_l.setOnClickListener(this);
        profile_layout.setOnClickListener(this);
        service_list.setOnClickListener(this);


        switch_show_protected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    Toast.makeText(appContext, "Notification On", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(appContext, "Notification Off", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void signoutApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(getContext(), Constant.uni_id ));
        RequestBody requestBody = builder.build();
        RetrofitService.Logout_user(new Dialog(getContext()), retrofitApiClient.Logout_user(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson(response);
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void parseJson(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject resObj = null;
            resObj = new JSONObject(response.body().string());
            Log.e("result", "" + resObj);
            String message = resObj.optString("message");
            String status = resObj.optString("status");
            if (status.equals("true")) {
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

                AppPreferences.clearAllPreferences(getContext());
                Intent intent = new Intent(getContext(), SignInActivity.class);
                startActivity(intent);
                getActivity().finish();

            }else {
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.add_product_l:
                Intent intent = new Intent(appContext, ProductListActivity.class);
                startActivity(intent);
                break;

            case R.id.signout_ll:
                Dialog();
                break;

            case R.id.profile_layout:
                Intent intent1 = new Intent(getActivity(), ProfileActivity.class);
                startActivity(intent1);
                break;

            case R.id.service_list:
                Intent intent11 = new Intent(getActivity(), ServiceListActivity.class);
                startActivity(intent11);
                break;
        }
    }

    void Dialog(){
        new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Log out")
                .setContentText("You will be returned to the login screen.")
                .setConfirmText("Logout!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        signoutApi();
                    }
                })
                .setCancelButton("Cancel", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                .show();
    }
}