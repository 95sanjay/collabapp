package com.info.collab.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;

import com.google.gson.Gson;
import com.info.collab.R;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.Alerts;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.BaseActivity;
import com.info.collab.utils.Constant;
import com.info.collab.utils.EmailChecker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class ForgotActivity extends BaseActivity implements View.OnClickListener {
    private Context appContext;
    EditText edt_email;
    EditText edt_otp;
    TextView btn_send,btn_submit;
    LinearLayout layout1 , layout2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        appContext = this;
        setContentView(R.layout.activity_forgot);
        Init();
    }

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    void Init() {
        edt_email = findViewById(R.id.edt_email);
        edt_otp = findViewById(R.id.edt_otp);
        btn_send = findViewById(R.id.btn_send);
        btn_submit = findViewById(R.id.btn_submit);
        layout1 = findViewById(R.id.layout1);
        layout2 = findViewById(R.id.layout2);

        btn_submit.setOnClickListener(this);
        btn_send.setOnClickListener(this);
        layout1.setVisibility(View.VISIBLE);
        layout2.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_send:
               // forgotServiceCall();
            {
                layout1.setVisibility(View.GONE);
                layout2.setVisibility(View.VISIBLE);
            }

                break;
            case R.id.btn_submit:
            {
                Intent intent = new Intent(this,SignInActivity.class);
                startActivity(intent);
                finish();
            }

                break;
        }
    }

    private void forgotServiceCall() {
        Map<String, Object> jsonParams = new HashMap<>();
        jsonParams.put("email",edt_email.getText().toString());
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                (new JSONObject(jsonParams)).toString());
/*
        RetrofitService.forgotP(new Dialog(appContext), retrofitApiClient.forgotPassword(body), new WebResponse() {
            @Override
            public void onResponseSuccess(Response<?> result) {
                Response<ResponseBody> response = (Response<ResponseBody>) result;

                parseJsonForgot(response);
            }

            @Override
            public void onResponseFailed(String error) {
                Alerts.showToast(appContext, error);
            }
        });*/
    }

    private void parseJsonForgot(Response<ResponseBody> response) {
        try {
            JSONObject jsonObject = new JSONObject(response.body().string());
            if (jsonObject.getString("success").equalsIgnoreCase("true")) {
            }
            Alerts.showToast(appContext, jsonObject.getString("success"));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
