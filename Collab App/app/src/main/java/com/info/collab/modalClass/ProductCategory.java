package com.info.collab.modalClass;

public class ProductCategory {
    private String category_identifier;
    private String category_english_name;
    private String category_arabic_name;
    private String category_description;
    private String category_main_image;
    private String created_at;
    private String updated_at;

    public String getCategory_main_image() {
        return category_main_image;
    }

    public void setCategory_main_image(String category_main_image) {
        this.category_main_image = category_main_image;
    }

    public String getCategory_description() {
        return category_description;
    }

    public void setCategory_description(String category_description) {
        this.category_description = category_description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCategory_identifier() {
        return category_identifier;
    }

    public void setCategory_identifier(String category_identifier) {
        this.category_identifier = category_identifier;
    }

    public String getCategory_english_name() {
        return category_english_name;
    }

    public void setCategory_english_name(String category_english_name) {
        this.category_english_name = category_english_name;
    }

    public String getCategory_arabic_name() {
        return category_arabic_name;
    }

    public void setCategory_arabic_name(String category_arabic_name) {
        this.category_arabic_name = category_arabic_name;
    }
}