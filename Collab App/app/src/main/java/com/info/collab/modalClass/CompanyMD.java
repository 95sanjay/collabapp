package com.info.collab.modalClass;

import org.json.JSONArray;

import java.io.Serializable;

public class CompanyMD implements Serializable {
    private String uni_id;
    private String business_email;
    private String business_main_phone_number;
    private String business_address;
    private String business_name;
    private String business_web_site;
    private String business_owner;
    private String business_bio;
    private String business_logo;
    private String business_cover_image;
    private String business_country;
    private String business_country_code;
    private String business_city;
    private String business_main_activity_id;
    private String business_sub_activity_id;
    private String shelf_doc;

    public String getUni_id() {
        return uni_id;
    }

    public void setUni_id(String uni_id) {
        this.uni_id = uni_id;
    }

    public String getBusiness_email() {
        return business_email;
    }

    public void setBusiness_email(String business_email) {
        this.business_email = business_email;
    }

    public String getBusiness_main_phone_number() {
        return business_main_phone_number;
    }

    public void setBusiness_main_phone_number(String business_main_phone_number) {
        this.business_main_phone_number = business_main_phone_number;
    }

    public String getBusiness_address() {
        return business_address;
    }

    public void setBusiness_address(String business_address) {
        this.business_address = business_address;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getBusiness_web_site() {
        return business_web_site;
    }

    public void setBusiness_web_site(String business_web_site) {
        this.business_web_site = business_web_site;
    }

    public String getBusiness_owner() {
        return business_owner;
    }

    public void setBusiness_owner(String business_owner) {
        this.business_owner = business_owner;
    }

    public String getBusiness_bio() {
        return business_bio;
    }

    public void setBusiness_bio(String business_bio) {
        this.business_bio = business_bio;
    }

    public String getBusiness_logo() {
        return business_logo;
    }

    public void setBusiness_logo(String business_logo) {
        this.business_logo = business_logo;
    }

    public String getBusiness_cover_image() {
        return business_cover_image;
    }

    public void setBusiness_cover_image(String business_cover_image) {
        this.business_cover_image = business_cover_image;
    }

    public String getBusiness_country() {
        return business_country;
    }

    public void setBusiness_country(String business_country) {
        this.business_country = business_country;
    }

    public String getBusiness_country_code() {
        return business_country_code;
    }

    public void setBusiness_country_code(String business_country_code) {
        this.business_country_code = business_country_code;
    }

    public String getBusiness_city() {
        return business_city;
    }

    public void setBusiness_city(String business_city) {
        this.business_city = business_city;
    }

    public String getBusiness_main_activity_id() {
        return business_main_activity_id;
    }

    public void setBusiness_main_activity_id(String business_main_activity_id) {
        this.business_main_activity_id = business_main_activity_id;
    }

    public String getBusiness_sub_activity_id() {
        return business_sub_activity_id;
    }

    public void setBusiness_sub_activity_id(String business_sub_activity_id) {
        this.business_sub_activity_id = business_sub_activity_id;
    }

    public String getShelf_doc() {
        return shelf_doc;
    }

    public void setShelf_doc(String shelf_doc) {
        this.shelf_doc = shelf_doc;
    }
}