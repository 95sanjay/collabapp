package com.info.collab.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.info.collab.R;
import com.info.collab.modalClass.CategoryMD;
import com.info.collab.modalClass.CollabMD;

import java.util.ArrayList;

public class CompanyCategoryAdapter extends RecyclerView.Adapter<CompanyCategoryAdapter.ImageViewHolder>implements Filterable {
    ArrayList<CategoryMD> imageArrayList;
    ArrayList<CategoryMD> imageArrayListFilterable;
    OnItemCollabClick listener;

    public CompanyCategoryAdapter(ArrayList<CategoryMD> imageArrayList, OnItemCollabClick listener) {
        this.imageArrayList = imageArrayList;
        this.imageArrayListFilterable = imageArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ImageViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.category_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int i) {
        CategoryMD product = imageArrayListFilterable.get(i);
        imageViewHolder.text_p_type.setText(product.getName());
        Glide.with(imageViewHolder.IType.getContext()).load(product.getImage()).into(imageViewHolder.IType);
        imageViewHolder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(product);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageArrayListFilterable.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder  {
        ImageView IType;
        TextView text_p_type;
        LinearLayout item;

        public ImageViewHolder(View itemView) {
            super(itemView);
            IType = itemView.findViewById(R.id.IType);
            text_p_type = itemView.findViewById(R.id.text_p_type);
            item = itemView.findViewById(R.id.item);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    imageArrayListFilterable = imageArrayList;
                } else {
                    ArrayList<CategoryMD> filteredList = new ArrayList<>();
                    for (CategoryMD row : imageArrayList) {
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    imageArrayListFilterable = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = imageArrayListFilterable;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                imageArrayListFilterable = (ArrayList<CategoryMD>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface OnItemCollabClick {
        void onItemClick(CategoryMD item);
    }
}
