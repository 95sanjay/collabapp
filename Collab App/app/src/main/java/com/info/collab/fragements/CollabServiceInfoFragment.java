package com.info.collab.fragements;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.info.collab.R;
import com.info.collab.adapter.ServiceListAdapter2;
import com.info.collab.modalClass.ServiceMD;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class CollabServiceInfoFragment extends BaseFragment implements View.OnClickListener {

    private View view;
    private RecyclerView feed_rv;
    private ArrayList<ServiceMD> servicelist;
    private ServiceListAdapter2 productAdapter;
    private TextView error_msg;

    public static CollabServiceInfoFragment newInstance(Bundle args) {
        CollabServiceInfoFragment fragment = new CollabServiceInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.collab_service_info, container, false);
        init();
        return view;
    }

    void  init(){
        feed_rv = view.findViewById(R.id.feed_rv);
        error_msg = view.findViewById(R.id.error_msg);
        productApi();
    }

    @Override
    public void onClick(View view) { }

    private void productApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(getContext(), Constant.uni_id ));
        RequestBody requestBody = builder.build();
        RetrofitService.Service_list(new Dialog(getContext()), retrofitApiClient.Service_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson(response);
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void parseJson(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject resObj = null;
            resObj = new JSONObject(response.body().string());
            String message = resObj.optString("message");
            String status = resObj.optString("status");
            JSONArray jsonArray = null;
            if (status.equals("true")) {
                jsonArray = resObj.getJSONArray("service_list");
                JSONObject object = null;
                servicelist = new ArrayList<>();
                for (int i = 0 ; i < jsonArray.length() ; i++){
                    object = jsonArray.getJSONObject(i);
                    ServiceMD product = new ServiceMD();
                    product.setId(object.getString("id"));
                    product.setCreated_by(object.getString("created_by"));
                    product.setEnglish_name(object.getString("english_name"));
                    product.setService_description(object.getString("service_description"));
                    product.setService_main_image(object.getString("service_main_image"));
                    product.setCategory(object.getString("category"));
                    product.setCreated_date_time(object.getString("created_date_time"));
                    servicelist.add(product);
                }
                productAdapter = new ServiceListAdapter2(servicelist, new ServiceListAdapter2.OnServiceClick() {
                    @Override
                    public void onServiceDelete(ServiceMD item) {

                    }

                    @Override
                    public void onServiceEdit(ServiceMD item) {

                    }
                });
                feed_rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                feed_rv.setItemAnimator(new DefaultItemAnimator());
                feed_rv.setAdapter(productAdapter);
                productAdapter.notifyDataSetChanged();
                error_msg.setVisibility(View.GONE);
            }else {
                error_msg.setVisibility(View.VISIBLE);
                error_msg.setText(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}