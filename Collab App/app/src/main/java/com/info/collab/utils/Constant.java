package com.info.collab.utils;

public class Constant {
    public static final String BASE_URL_MAIN = "http://tenspark.com/collab/";
    public static final String BASE_URL = BASE_URL_MAIN + "api/";
    public static final String SIGN_UP = "registration";
    public static final String SIGN_IN = "login_user";
    //public static final String FORGOT = "forgot";
    public static final String get_product_list = "get_product_list";
    public static final String get_category_list = "get_category_list";
    public static final String get_subcategory_list = "get_subcategory_list";
    public static final String live_feed = "live_feed";
    public static final String add_product = "add_product";
    public static final String logout_user = "logout_user";
    public static final String post = "post";
    public static final String get_chat_list = "get_chat_list";
    public static final String get_userchat_list = "get_userchat_list";
    public static final String product_info = "product_info";
    public static final String getNewInProduct = "getNewInProduct";
    public static final String categorylist = "categorylist";
    public static final String get_company_list = "get_company_list";
    public static final String filter = "filter";
    public static final String add_service = "add_service";
    public static final String get_service_list = "get_service_list";
    public static final String delete_product = "delete_product";
    public static final String delete_service = "delete_service";
    public static final String edit_product = "edit_product";
    public static final String edit_service = "edit_service";
    public static final String registration = "registration";
    public static final String edit_user = "edit_user";
    public static final String chating = "chating";
    public static final String delete_live_feed = "delete_live_feed";
    public static final String post_comment = "post_comment";
    public static final String get_category_products = "get_category_products";
    public static final String post_like = "post_like";
    public static final String get_company_details = "get_company_details";
    public static final String get_company_subcategory_list = "get_company_subcategory_list";
    public static final String get_company_category_list = "get_company_category_list";
    public static final String get_state_list = "get_state_list";
    public static final String get_city_list = "get_city_list";
    public static final String business_list = "business_list";
    public static final String imageurl = "imageurl";
    public static final String follow = "follow";
    public static final String get_subcategory_products = "get_subcategory_products";

    public static final String SET_CALENDER_PROPERTY_UPDATE = "add_order_owner";


    public static final String PROPERTY_UPCOMING = "getUserIdPropertyOlderUpcoming";


    public static final String BILLING_LIST = "get_billinglist";
    public static final String GIVE_RATING = "give_rating";


    public static final String mypropertiesURL = "myproperties";

    ///////////////////////////  login  ///////////////////

    public static final String uni_id = "uni_id";
    public static final String collab_uni_id = "collab_uni_id";
    public static final String collab_token_id = "uni_id";
    public static final String BID = "BID";
    public static final String business_email = "business_email";
    public static final String business_main_phone_number = "business_main_phone_number";
    public static final String business_address = "business_address";
    public static final String business_lat = "business_lat";
    public static final String business_lng = "business_lng";
    public static final String business_web_site = "business_web_site";
    public static final String business_name = "business_name";
    public static final String business_username = "business_username";
    public static final String business_owner = "business_owner";
    public static final String business_password = "business_password";
    public static final String business_account_status = "business_account_status";
    public static final String business_bio = "business_bio";
    public static final String business_logo = "business_logo";
    public static final String business_cover_image = "business_cover_image";
    public static final String business_country = "business_country";
    public static final String business_country_code = "business_country_code";
    public static final String business_city = "business_city";
    public static final String business_main_activity_id = "business_main_activity_id";
    public static final String business_sub_activity_id = "business_sub_activity_id";
    public static final String authentic_doc = "authentic_doc";
    public static final String doc_type = "doc_type";
    public static final String shelf_doc = "shelf_doc";
    public static final String business_creation_date = "business_creation_date";
    public static final String business_subscriptions_renew_date = "business_subscriptions_renew_date";
    public static final String business_exp_date = "business_exp_date";
    public static final String token_id = "token_id";
    public static final String created_at = "created_at";
    public static final String updated_at = "updated_at";

    public static final String IS_LOGIN = "IS_LOGIN";
    public static final String IS_WITHOUT_LOGIN = "IS_WITHOUT_LOGIN";

    public static final String device_id = "device_id";

    public static final String per_week_charge = "per_week_charge";
    public static final String per_month_charge = "per_month_charge";


    public static final String cleaning_charge = "cleaning_charge";
    public static final String service_charge = "service_charge";
    public static final String no_of_rooms = "no_of_rooms";
    public static final String no_of_kitchens = "no_of_kitchens";
    public static final String no_of_bathrooms = "no_of_bathrooms";
    public static final String no_of_guests = "no_of_guests";
    public static final String no_bedrooms = "no_bedrooms";

    public static final String no_bathrooms = "no_bathrooms";
    public static final String bathroom_type = "bathroom_type";
    public static final String amenities_charge = "amenities_charge";
    public static final String no_of_bad = "no_of_bad";
    public static final String ownerid = "ownerid";
    public static final String p_amenities = "p_amenities";
    public static final String p_safetyprivacy = "p_safetyprivacy";
    public static final String mobile_public = "mobile_public";
    public static final String images = "images";
    public static final String pimage = "pimage";
    public static final String badrooms = "badrooms";
    public static final String reviewsuser = "reviewsuser";
    public static final String similar = "similar";
    public static final String slider = "slider";

    public static final String total_reviewpeople = "total_reviewpeople";
    public static final String agvrating = "agvrating";
    public static final String GET_PROPERTY_TYPE = "get_property_type";
    public static final String PACKAGE_NAME = "lak.com.bookingapp";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";
    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String RESULT_DATA_KEY = PACKAGE_NAME +".RESULT_DATA_KEY";
    public static final String PLACE_ID = PACKAGE_NAME +  ".PLACE_ID";
    public static final String RESULT_COUNTRY_CODE_KEY = PACKAGE_NAME + ".COUNTRY_CODE";
    public static final int REQUEST_CHECK_SETTINGS = 3;

    public static final String action = "action";

    /////Fragment

    public static final String MySpaceFragment_Tag = "MySpaceFragment";
    public static final int MySpaceFragment_Id = 1;


    public static final String progress_type = "progress_type";


    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";
}
