package com.info.collab.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.info.collab.R;
import com.info.collab.adapter.ProductListAdapter;
import com.info.collab.modalClass.Product;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.BaseActivity;
import com.info.collab.utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class ProductListActivity extends BaseActivity implements View.OnClickListener{

    RecyclerView feed_rv;
    private ArrayList<Product> productslist;
    private ProductListAdapter productAdapter;
    ImageView addBtn,backBtn;

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_list);
        setStatusBarGradiant(this);
        init();
    }

    void init(){
        feed_rv = findViewById(R.id.feed_rv);
        backBtn = findViewById(R.id.backBtn);
        addBtn = findViewById(R.id.addBtn);
        backBtn.setOnClickListener(this);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductListActivity.this, AddProductActivity.class);
                startActivityForResult(intent,111);
            }
        });
        productApi();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111 && resultCode == 222) {
            productApi();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.backBtn:
                onBackPressed();
                break;
        }
    }

    private void productApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(getApplicationContext(), Constant.uni_id ))
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(getApplicationContext(), Constant.token_id ));

        RequestBody requestBody = builder.build();
        RetrofitService.Product_List(new Dialog(ProductListActivity.this), retrofitApiClient.Product_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson(response);
            }

            @Override
            public void onResponseFailed(String error) {
            }
        });
    }

    private void parseJson(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject resObj = null;
            resObj = new JSONObject(response.body().string());
            String message = resObj.optString("message");
            String status = resObj.optString("status");
            JSONArray jsonArray = null;
            if (status.equals("true")) {
                jsonArray = resObj.getJSONArray("product_list");
                JSONObject object = null;
                productslist = new ArrayList<>();
                for (int i = 0 ; i < jsonArray.length() ; i++){
                    object = jsonArray.getJSONObject(i);
                    Product product = new Product();
                    product.setProduct_identifier(object.getString("product_identifier"));
                    product.setBID(object.getString("BID"));
                    product.setHs_code(object.getString("hs_code"));
                    product.setSeller_type(object.getString("seller_type"));
                    product.setArabic_name(object.getString("arabic_name"));
                    product.setEnglish_name(object.getString("english_name"));
                    product.setProduct_category_id(object.getString("product_category_id"));
                    product.setProduct_sub_category_id(object.getString("product_sub_category_id"));
                    product.setProduct_description(object.getString("product_description"));
                    product.setProduct_main_image(object.getString("product_main_image"));
                    product.setProduct_creation_date(object.getString("product_creation_date"));
                    productslist.add(product);
                }
                productAdapter = new ProductListAdapter(productslist, new ProductListAdapter.OnProductClick() {
                    @Override
                    public void onProductDelete(Product item) {
                        deleteApi(item.getProduct_identifier());
                    }

                    @Override
                    public void onProductEdit(Product item , String str) {

                        if (str.equalsIgnoreCase("next")){
                            Intent intent = new Intent(ProductListActivity.this,ProductInfo.class);
                            intent.putExtra("product_id",item.getProduct_identifier());
                            startActivity(intent);
                        }else {
                            Intent i = new Intent(ProductListActivity.this,UpdateProductActivity.class);
                            i.putExtra("product", item);
                            startActivityForResult(i,111);
                        }
                    }
                });
                feed_rv.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                feed_rv.setItemAnimator(new DefaultItemAnimator());
                feed_rv.setAdapter(productAdapter);
                productAdapter.notifyDataSetChanged();
            }else {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {

        }
    }

    private void deleteApi(String id){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(getApplicationContext(), Constant.uni_id ))
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(getApplicationContext(), Constant.token_id ))
                .addFormDataPart("product_id",id);

        RequestBody requestBody = builder.build();
        RetrofitService.Delete_product(new Dialog(ProductListActivity.this), retrofitApiClient.delete_product(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson2(response);
            }

            @Override
            public void onResponseFailed(String error) {
            }
        });
    }

    private void parseJson2(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject jsonObject = new JSONObject(response.body().string());
            if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                Toast.makeText(this,jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                productApi();
            }
        } catch (Exception e) {

        }
    }
}