package com.info.collab.modalClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class UserClient {

@SerializedName("status")
@Expose
private String status;
@SerializedName("message")
@Expose
private String message;

/**
* No args constructor for use in serialization
*
*/
public UserClient() {
}

/**
*
* @param message
* @param status
*/
public UserClient(String status, String message) {
super();
this.status = status;
this.message = message;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

}