package com.info.collab.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.info.collab.R;
import com.info.collab.modalClass.Product;
import com.info.collab.modalClass.ServiceMD;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.BaseActivity;
import com.info.collab.utils.Constant;

import org.json.JSONObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class ServiceInfo extends BaseActivity implements View.OnClickListener{

    ImageView cancelBtn,backBtn,product_img;
    private String product_id;
    private ImageView chat_btn;
    TextView discripitionTV,categoryTV,product_nameTV;
    private String user2_id;
    private ServiceMD service;
    private TextView title;

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_info);
        setStatusBarGradiant(this);
        init();
        Intent i = getIntent();
        service = (ServiceMD) i.getSerializableExtra("service");
        if (i != null){
            Log.d("sanjServ",""+service.getBusiness_name());
            product_nameTV.setText(service.getEnglish_name());
            categoryTV.setText("Electronics");
            discripitionTV.setText(service.getService_description());
            title.setText("Service");
            Glide.with(product_img)
                    .asBitmap()
                    .load(service.getService_main_image())
                    .error(R.drawable.demo)
                    .into(product_img);
            TextView tt1 = findViewById(R.id.tt1);
            TextView tt2 = findViewById(R.id.tt2);
            tt1.setText(service.getBusiness_name());
            tt2.setText(service.getBusiness_address());
        }
        backBtn.setOnClickListener(view -> onBackPressed());
    }

    void init(){
        product_img = findViewById(R.id.product_img);
        discripitionTV = findViewById(R.id.discripitionTV);
        categoryTV = findViewById(R.id.categoryTV);
        product_nameTV = findViewById(R.id.product_nameTV);
        backBtn = findViewById(R.id.backBtn);
        title = findViewById(R.id.title);
        chat_btn = findViewById(R.id.chat_btn);
        chat_btn.setOnClickListener(view -> {
            Intent intent = new Intent(this,ChatActivity.class);
            intent.putExtra("user2_id",service.getCreated_by_id());
            startActivity(intent);
        });
    }

    @Override
    public void onClick(View view) { }

}