package com.info.collab.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.gson.Gson;
import com.info.collab.R;
import com.info.collab.adapter.CategoryAdapter;
import com.info.collab.adapter.CityAdapter;
import com.info.collab.adapter.StateAdapter;
import com.info.collab.adapter.SubActivityAdapter;
import com.info.collab.modalClass.CategoryMD;
import com.info.collab.modalClass.ChatMD;
import com.info.collab.modalClass.CityMD;
import com.info.collab.modalClass.StateMD;
import com.info.collab.modalClass.SubCategoryMD;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.Alerts;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.BaseActivity;
import com.info.collab.utils.Constant;
import com.info.collab.utils.MyDatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class Signup2Activity extends BaseActivity implements View.OnClickListener {
    private Context appContext;
    TextView btn_sign_in2;
    private Spinner main_spin, sub_spin,spin_city,spin_state;
    private ImageView cover_image, auth_doc_Img;
    CircleImageView logoImg;
    EditText b_email_edt, b_country_edt,
            business_owner_edt, b_establish_edt1,city_edt,bio_edt,
            data_type_edt,expiry_date;
    public final int REQUEST_CODE_PICKER = 125;
    public final int REQUEST_CODE_PICKER2 = 12256;
    public final int REQUEST_CODE_PICKER3 = 12236556;
    ArrayList<Image> images = new ArrayList<>();
    ArrayList<Image> images2 = new ArrayList<>();
    ArrayList<Image> images3 = new ArrayList<>();
    String email, phone, b_name, location, pass, con_pass;
    private String main_activity,sub_activity;

    private byte[] imageBytes3;
    private String timeStamp3="";
    private String extension3="";

    private byte[] imageBytes1;
    private String timeStamp1="";
    private String extension1="";

    private byte[] imageBytes2;
    private String timeStamp2="";
    private String extension2="";

    private String divice_id;
    private String city;
    private ArrayList<CategoryMD> categoryMDArrayList;
    private ArrayList<CategoryMD> categoryMDArrayList2 = new ArrayList<>();
    private ArrayList<SubCategoryMD> subCategoryMDArrayList;
    private ArrayList<SubCategoryMD> subCategoryMDArrayList2 = new ArrayList<>();
    private ArrayList<StateMD> StateList;
    private ArrayList<StateMD> StateList2 = new ArrayList<>();
    private ArrayList<CityMD> CityList;
    private ArrayList<CityMD> CityList2 = new ArrayList<>();
    private String state;
    private ImageView backBtn;

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        appContext = this;
        setContentView(R.layout.fragment_bussiness_info);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            email = bundle.getString("email", "");
            phone = bundle.getString("phone", "");
            b_name = bundle.getString("b_name", "");
            location = bundle.getString("location", "");
            pass = bundle.getString("pass", "");
            con_pass = bundle.getString("con_pass", "");
        }
        SetupUI();
    }

    private void SetupUI() {
        bio_edt = findViewById(R.id.bio_edt);
        b_country_edt = findViewById(R.id.country_edt);
        business_owner_edt = findViewById(R.id.business_owner_edt);
        b_establish_edt1 = findViewById(R.id.establish_edt);
        spin_city = findViewById(R.id.spin_city);
        spin_state = findViewById(R.id.spin_state);
        data_type_edt = findViewById(R.id.data_type_edt);
        expiry_date = findViewById(R.id.expiry_date);
        sub_spin = findViewById(R.id.sub_spin);
        backBtn = findViewById(R.id.backBtn);

        btn_sign_in2 = findViewById(R.id.btn_sign_in2);
        cover_image = findViewById(R.id.cover_image);
        logoImg = findViewById(R.id.logoImg);
        auth_doc_Img = findViewById(R.id.auth_doc_Img);
        main_spin = findViewById(R.id.main_spin1);

        btn_sign_in2.setOnClickListener(this);
        cover_image.setOnClickListener(this);
        logoImg.setOnClickListener(this);
        b_establish_edt1.setOnClickListener(this);
        auth_doc_Img.setOnClickListener(this);
        expiry_date.setOnClickListener(this);
        backBtn.setOnClickListener(this);

        divice_id = AppPreferences.getStringPreference(Signup2Activity.this, Constant.device_id );
        categouryApi();
        stateApi();
    }

    private boolean isInputValid() {
        if (images3.size() == 0) {
            Alerts.showToast(this, "Please select Auth doc image.");
            return false;
        }else if (images.size() == 0) {
            Alerts.showToast(this, "Please select cover image.");
            return false;
        }else if (images2.size() == 0) {
            Alerts.showToast(this, "Please select logo.");
            return false;
        } else if (bio_edt.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter bio.");
            return false;
        } /*else if (b_country_code_edt.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter country coade.");
            return false;
        } else if (b_country_edt.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter country.");
            return false;
        } */else if (city.equalsIgnoreCase("Select")) {
            Alerts.showToast(this, "Please select city.");
            return false;
        } else if (business_owner_edt.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter business name.");
            return false;
        } else if (b_establish_edt1.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter establish date.");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_in2:
                if (isInputValid()) {
                    uploadImagenew();
                }
                break;

            case R.id.backBtn:
                onBackPressed();
                break;

            case R.id.cover_image:
                selectImg();
                break;

            case R.id.logoImg:
                selectlogo();
                break;

            case R.id.auth_doc_Img:
                selectauth();
                break;

            case R.id.establish_edt:
            {
                MyDatePickerDialog dialog = new MyDatePickerDialog(this);

                dialog.setTitle("Set Date");
                dialog.showDatePicker(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Log.d("dateTime",""+year+"-"+month+"-"+dayOfMonth);
                        b_establish_edt1.setText(year+"-"+month+"-"+dayOfMonth);
                        //Date select callback
                    }
                }, Calendar.getInstance());
            }
                break;

            case R.id.expiry_date:
                MyDatePickerDialog dialog = new MyDatePickerDialog(this);
                dialog.setTitle("Set Date");
                dialog.showDatePicker(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Log.d("dateTime",""+year+"-"+month+"-"+dayOfMonth);
                        expiry_date.setText(year+"-"+month+"-"+dayOfMonth);
                    }
                }, Calendar.getInstance());
                break;
        }
    }

    void selectImg() {
        ImagePicker.create(Signup2Activity.this)
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .theme(R.style.ImagePickerTheme)
                .single() // max images can be selected (99 by default)
                .showCamera(false) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image ("Camera" folder by default)
                .origin(images) // original selected images, used in multi mode
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }

    void selectlogo() {
        ImagePicker.create(Signup2Activity.this)
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .theme(R.style.ImagePickerTheme)
                .single() // max images can be selected (99 by default)
                .showCamera(false) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image ("Camera" folder by default)
                .origin(images2) // original selected images, used in multi mode
                .start(REQUEST_CODE_PICKER2); // start image picker activity with request code
    }

    void selectauth() {
        ImagePicker.create(Signup2Activity.this)
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .theme(R.style.ImagePickerTheme)
                .single() // max images can be selected (99 by default)
                .showCamera(false) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image ("Camera" folder by default)
                .origin(images3) // original selected images, used in multi mode
                .start(REQUEST_CODE_PICKER3); // start image picker activity with request code
    }

    @Override
    public void onActivityResult(int requestCode, final int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICKER && resultCode == Activity.RESULT_OK && data != null) {
            images.clear();
            images.addAll(ImagePicker.getImages(data));
            if (images.size() > 0) {

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeFile(images.get(0).getPath());
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                imageBytes1 = baos.toByteArray();
                String imageString = Base64.encodeToString(imageBytes1, Base64.DEFAULT);
                Long tsLong = System.currentTimeMillis() / 1000;
                timeStamp1 = tsLong.toString();
                String filenameArray[] = images.get(0).getName().split("\\.");
                extension1 = filenameArray[filenameArray.length - 1];

                cover_image.setImageBitmap(bitmap);
            }
        } else if (requestCode == REQUEST_CODE_PICKER2 && resultCode == Activity.RESULT_OK && data != null) {
            images2.clear();
            images2.addAll(ImagePicker.getImages(data));
            if (images2.size() > 0) {

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeFile(images2.get(0).getPath());
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                imageBytes2 = baos.toByteArray();
                String imageString = Base64.encodeToString(imageBytes2, Base64.DEFAULT);
                Long tsLong = System.currentTimeMillis() / 1000;
                timeStamp2 = tsLong.toString();
                String filenameArray[] = images2.get(0).getName().split("\\.");
                extension2 = filenameArray[filenameArray.length - 1];

                logoImg.setImageBitmap(bitmap);
            }
        }else if (requestCode == REQUEST_CODE_PICKER3 && resultCode == Activity.RESULT_OK && data != null) {
            images3.clear();
            images3.addAll(ImagePicker.getImages(data));
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeFile(images3.get(0).getPath());
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                imageBytes3 = baos.toByteArray();
                String imageString = Base64.encodeToString(imageBytes3, Base64.DEFAULT);
                Long tsLong = System.currentTimeMillis() / 1000;
                timeStamp3 = tsLong.toString();
                String filenameArray[] = images3.get(0).getName().split("\\.");
                extension3 = filenameArray[filenameArray.length - 1];
                auth_doc_Img.setImageBitmap(bitmap);
            }
        }

    private void stateApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("country_id", "101");
        RequestBody requestBody = builder.build();
        RetrofitService.State_list(new Dialog(Signup2Activity.this), retrofitApiClient.State_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("states");
                        JSONObject object = null;
                        StateList = new ArrayList<>();
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            object = jsonArray.getJSONObject(i);
                            StateMD categoryMD = new StateMD();
                            categoryMD.setId(object.getString("id"));
                            categoryMD.setName(object.getString("name"));
                            StateList.add(categoryMD);
                        }
                        StateMD categoryMD = new StateMD();
                        categoryMD.setId("0");
                        categoryMD.setName("Select");
                        StateList2.add(categoryMD);
                        StateList2.addAll(StateList);

                        StateAdapter categoryAdapter = new StateAdapter(getApplicationContext(),StateList2);
                        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spin_state.setAdapter(categoryAdapter);
                        spin_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                state = StateList2.get(i).getId();
                             if (state.equalsIgnoreCase("0")){
                                    CityList2.clear();
                                    CityMD categoryMD = new CityMD();
                                    categoryMD.setId("0");
                                    categoryMD.setName("Select");
                                    CityList2.add(categoryMD);
                                    CityAdapter categoryAdapter = new CityAdapter(getApplicationContext(),CityList2);
                                    categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spin_city.setAdapter(categoryAdapter);
                                }else {
                                 cityApi(state);
                             }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                }catch (Exception e){}
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void cityApi(String id){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("state_id", id);
        RequestBody requestBody = builder.build();
        RetrofitService.City_list(new Dialog(Signup2Activity.this), retrofitApiClient.City_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("states");
                        JSONObject object = null;
                        CityList = new ArrayList<>();
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            object = jsonArray.getJSONObject(i);
                            CityMD categoryMD = new CityMD();
                            categoryMD.setId(object.getString("id"));
                            categoryMD.setName(object.getString("name"));
                            CityList.add(categoryMD);
                        }
                        CityAdapter categoryAdapter = new CityAdapter(getApplicationContext(),CityList);
                        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spin_city.setAdapter(categoryAdapter);
                        spin_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                city = CityList.get(i).getId();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                }catch (Exception e){}
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void categouryApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", "1");
        RequestBody requestBody = builder.build();
        RetrofitService.Company_category_list(new Dialog(Signup2Activity.this), retrofitApiClient.Company_category_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("data");
                        JSONObject object = null;
                        categoryMDArrayList = new ArrayList<>();
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            object = jsonArray.getJSONObject(i);
                            CategoryMD categoryMD = new CategoryMD();
                            categoryMD.setId(object.getString("id"));
                            categoryMD.setName(object.getString("name"));
                            categoryMDArrayList.add(categoryMD);
                        }
                        CategoryMD categoryMD = new CategoryMD();
                        categoryMD.setId("0");
                        categoryMD.setName("Select");
                        categoryMDArrayList2.add(categoryMD);
                        categoryMDArrayList2.addAll(categoryMDArrayList);
                        CategoryAdapter categoryAdapter = new CategoryAdapter(getApplicationContext(),categoryMDArrayList2);
                        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        main_spin.setAdapter(categoryAdapter);
                        main_spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                main_activity = categoryMDArrayList2.get(i).getId();
                                if (main_activity.equalsIgnoreCase("0")){
                                    subCategoryMDArrayList2.clear();
                                    SubCategoryMD subCategoryMD = new SubCategoryMD();
                                    subCategoryMD.setId("0");
                                    subCategoryMD.setName("Select");
                                    subCategoryMDArrayList2.add(subCategoryMD);
                                    SubActivityAdapter subActivityAdapter = new SubActivityAdapter(getApplicationContext(),subCategoryMDArrayList2);
                                    subActivityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    sub_spin.setAdapter(subActivityAdapter);
                                }else {
                                    subcategouryApi(main_activity);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                }catch (Exception e){}
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void subcategouryApi(String id){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("category_id", id);
        RequestBody requestBody = builder.build();
        RetrofitService.Company_subcategory_list(new Dialog(Signup2Activity.this), retrofitApiClient.Company_subcategory_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("subcategory_list");
                        JSONObject object = null;
                        subCategoryMDArrayList = new ArrayList<>();
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            object = jsonArray.getJSONObject(i);
                            SubCategoryMD subCategoryMD = new SubCategoryMD();
                            subCategoryMD.setId(object.getString("id"));
                            subCategoryMD.setName(object.getString("name"));
                            subCategoryMDArrayList.add(subCategoryMD);
                        }
                        SubActivityAdapter subActivityAdapter = new SubActivityAdapter(getApplicationContext(),subCategoryMDArrayList);
                        subActivityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sub_spin.setAdapter(subActivityAdapter);
                        sub_spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                sub_activity = subCategoryMDArrayList.get(i).getId();
                                subCategoryMDArrayList2.clear();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                }catch (Exception e){}
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void uploadImagenew(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("name", b_name)
                .addFormDataPart("username", email)
                .addFormDataPart("ownername", business_owner_edt.getText().toString().trim())
                .addFormDataPart("email_id", email)
                .addFormDataPart("phone_no", phone)
                .addFormDataPart("address", location)
                .addFormDataPart("password", pass)
                .addFormDataPart("bio",bio_edt.getText().toString().trim())
                .addFormDataPart("doc_type", data_type_edt.getText().toString().trim())
                .addFormDataPart("country", b_country_edt.getText().toString().trim())
                .addFormDataPart("country_code", "+91")
                .addFormDataPart("city", city)
                .addFormDataPart("main_activity", main_activity)
                .addFormDataPart("sub_activity", sub_activity)
                .addFormDataPart("creation_date", b_establish_edt1.getText().toString().trim())
                .addFormDataPart("expiry_date", expiry_date.getText().toString().trim())
                .addFormDataPart("device_id", "testing"+divice_id);

        if (images.size()>0){
            File featured_image = new File(images.get(0).getPath());
            if (featured_image.exists()) {
                builder.addFormDataPart("cover_img", featured_image.getName(), RequestBody.create(MultipartBody.FORM, featured_image));
            }
        }
        if (images2.size()>0){
            File featured_image = new File(images2.get(0).getPath());
            if (featured_image.exists()) {
                builder.addFormDataPart("logo", featured_image.getName(), RequestBody.create(MultipartBody.FORM, featured_image));
            }
        }
        if (images3.size()>0){
            File featured_image = new File(images3.get(0).getPath());
            if (featured_image.exists()) {
                builder.addFormDataPart("shelf_doc", featured_image.getName(), RequestBody.create(MultipartBody.FORM, featured_image));
                builder.addFormDataPart("auth_doc[]", featured_image.getName(), RequestBody.create(MultipartBody.FORM, featured_image));
            }
        }

        RequestBody requestBody = builder.build();
        RetrofitService.getPostCreateBodyResponse(new Dialog(Signup2Activity.this), retrofitApiClient.Registration(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson2(response);
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void parseJson2(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject jsonObject = new JSONObject(response.body().string());
            if (jsonObject.getString("status").equalsIgnoreCase("true")) {
               // Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                signInServiceCall(email, pass);
            }else {
                Toast.makeText(appContext,jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {

        }
    }

    private void signInServiceCall(String userEmail, String userPassword) {
        Map<String, Object> jsonParams = new HashMap<>();
        jsonParams.put("email_id",userEmail  );
        jsonParams.put("password", userPassword);

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                (new JSONObject(jsonParams)).toString());

        RetrofitService.signIn(new Dialog(appContext), retrofitApiClient.signIn(body), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;

                parseJson(response);
            }

            @Override
            public void onResponseFailed(String error) {
                Alerts.showToast(appContext, error);
            }
        });
    }

    private void parseJson(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject jsonObject = new JSONObject(response.body().string());


            if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                JSONObject object = jsonObject.getJSONObject("data");
                String userData = new Gson().toJson(object);

                AppPreferences.setStringPreference(appContext, Constant.uni_id, object.getString(Constant.uni_id));
                AppPreferences.setStringPreference(appContext, Constant.BID, object.getString(Constant.BID));
                AppPreferences.setStringPreference(appContext, Constant.business_email, object.getString(Constant.business_email));
                AppPreferences.setStringPreference(appContext, Constant.business_main_phone_number, object.getString(Constant.business_main_phone_number));
                AppPreferences.setStringPreference(appContext, Constant.business_address, object.getString(Constant.business_address));
                AppPreferences.setStringPreference(appContext, Constant.business_lat, object.getString(Constant.business_lat));
                AppPreferences.setStringPreference(appContext, Constant.business_lng, object.getString(Constant.business_lng));
                AppPreferences.setStringPreference(appContext, Constant.business_web_site, object.getString(Constant.business_web_site));
                AppPreferences.setStringPreference(appContext, Constant.business_name, object.getString(Constant.business_name));
                AppPreferences.setStringPreference(appContext, Constant.business_username, object.getString(Constant.business_username));
                AppPreferences.setStringPreference(appContext, Constant.business_owner, object.getString(Constant.business_owner));
                AppPreferences.setStringPreference(appContext, Constant.business_password, object.getString(Constant.business_password));
                AppPreferences.setStringPreference(appContext, Constant.business_account_status, object.getString(Constant.business_account_status));
                AppPreferences.setStringPreference(appContext, Constant.business_bio, object.getString(Constant.business_bio));
                AppPreferences.setStringPreference(appContext, Constant.business_logo, object.getString(Constant.business_logo));
                AppPreferences.setStringPreference(appContext, Constant.business_cover_image, object.getString(Constant.business_cover_image));
                AppPreferences.setStringPreference(appContext, Constant.business_country, object.getString(Constant.business_country));
                AppPreferences.setStringPreference(appContext, Constant.business_country_code, object.getString(Constant.business_country_code));
                AppPreferences.setStringPreference(appContext, Constant.business_city, object.getString(Constant.business_city));
                AppPreferences.setStringPreference(appContext, Constant.business_main_activity_id, object.getString(Constant.business_main_activity_id));
                AppPreferences.setStringPreference(appContext, Constant.business_sub_activity_id, object.getString(Constant.business_sub_activity_id));
                AppPreferences.setStringPreference(appContext, Constant.authentic_doc, object.getString(Constant.authentic_doc));
                AppPreferences.setStringPreference(appContext, Constant.doc_type, object.getString(Constant.doc_type));
                AppPreferences.setStringPreference(appContext, Constant.shelf_doc, object.getString(Constant.shelf_doc));
                AppPreferences.setStringPreference(appContext, Constant.business_creation_date, object.getString(Constant.business_creation_date));
                AppPreferences.setStringPreference(appContext, Constant.business_subscriptions_renew_date, object.getString(Constant.business_subscriptions_renew_date));
                AppPreferences.setStringPreference(appContext, Constant.business_exp_date, object.getString(Constant.business_exp_date));
                AppPreferences.setStringPreference(appContext, Constant.token_id, object.getString(Constant.token_id));
                AppPreferences.setStringPreference(appContext, Constant.created_at, object.getString(Constant.created_at));
                AppPreferences.setStringPreference(appContext, Constant.updated_at, object.getString(Constant.updated_at));
                startSession();
            } else {
                Alerts.showToast(appContext, "Invalid Email or Password");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startSession() {
        AppPreferences.setBooleanPreference(appContext, Constant.IS_LOGIN, true);
        AppPreferences.setBooleanPreference(appContext, Constant.IS_WITHOUT_LOGIN, false);
        Alerts.showToast(appContext, "Login success");
        // SplashActivity.setUserModel(appContext);

        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
        startActivity(new Intent(appContext, HomeActivity.class));
        finish();
    }
}
