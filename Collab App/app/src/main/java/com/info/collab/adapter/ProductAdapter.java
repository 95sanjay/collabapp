package com.info.collab.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.info.collab.R;
import com.info.collab.modalClass.Product;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ImageViewHolder> {
    ArrayList<Product> imageArrayList;
    private final OnItemNewInClick listener;
    Context context;

    public ProductAdapter(Context context,ArrayList<Product> imageArrayList, OnItemNewInClick listener) {
        this.imageArrayList = imageArrayList;
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ImageViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pro_type, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int i) {
        Product product = imageArrayList.get(i);

        imageViewHolder.text_p_type.setText(product.getEnglish_name());

        Glide.with(imageViewHolder.IType.getContext()).asBitmap().load(product.getProduct_main_image()).apply(new RequestOptions().override(120, 78)).error(R.drawable.test_poster).into(imageViewHolder.IType);

        imageViewHolder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(product);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageArrayList.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder  {
        ImageView IType;
        TextView text_p_type;
        LinearLayout item;

        public ImageViewHolder(View itemView) {
            super(itemView);
            IType = itemView.findViewById(R.id.IType);
            item = itemView.findViewById(R.id.item);
            text_p_type = itemView.findViewById(R.id.text_p_type);
        }
    }

    public interface OnItemNewInClick {
        void onItemClick(Product item);
    }
}
