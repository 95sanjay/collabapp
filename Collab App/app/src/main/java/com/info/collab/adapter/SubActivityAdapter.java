package com.info.collab.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.info.collab.R;
import com.info.collab.modalClass.CategoryMD;
import com.info.collab.modalClass.SubCategoryMD;

import java.util.ArrayList;

public class SubActivityAdapter extends ArrayAdapter<SubCategoryMD> {

    public SubActivityAdapter(Context context, ArrayList<SubCategoryMD> countryList) {
        super(context, 0, countryList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.row_items, parent, false
            );
        }
        TextView textViewName = convertView.findViewById(R.id.text_view_name);

        SubCategoryMD currentItem = getItem(position);

        if (currentItem != null) {
            textViewName.setText(currentItem.getName());
        }

        return convertView;
    }
}