package com.info.collab.retrofits_provider;


import android.app.Dialog;
import android.util.Log;
import android.util.Property;

import com.info.collab.utils.Alerts;
import com.info.collab.utils.Constant;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitService {
    public static ApiClient client;
    private static volatile Retrofit retrofit = null;

    public static ApiClient getRxClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constant.BASE_URL)
                .build();
        return retrofit.create(ApiClient.class);
    }

    public RetrofitService() {

        synchronized (RetrofitService.class) {
            // Double check
            if (retrofit == null) {
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger
                        () {
                    @Override
                    public void log(String message) {
                        Log.e("OK HTTP", message);
                    }
                });
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                OkHttpClient client = new OkHttpClient.Builder()
                        .addInterceptor(interceptor)
                        .readTimeout(1800, TimeUnit.SECONDS)
                        .connectTimeout(1800, TimeUnit.SECONDS)
                        .writeTimeout(1800, TimeUnit.SECONDS)
                        .build();
                retrofit = new Retrofit.Builder()
                        .baseUrl(Constant.BASE_URL)
                        .client(client)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
        }
        client = retrofit.create(ApiClient.class);
    }

    public static ApiClient getRetrofit() {
        if (client == null)
            new RetrofitService();

        return client;
    }


    public static void singUpnew(Dialog dialog, Call<ResponseBody> method, WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void singUp(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void feed_post(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void getPostCreateBodyResponse(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Edit_user(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Live_feed(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Userchat_list(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Company_subcategory_list(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        //Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Subcategory_products(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        //Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Delete_live_feed(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Post_comment(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
       // Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Post_like(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
      //  Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Follow(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
      //  Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Logout_user(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Chat_list(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
       // Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Chating(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
       // Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Registration(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Update_service(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void addProduct(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Update_product(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Product_List(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
       // Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Company_details(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
       // Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Business_list(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Category_products(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void NewInProduct_list(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Category_list(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Company_category_list(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
       // Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void State_list(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
       // Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void City_list(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
       // Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Collab_list(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Filter(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Service_list(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
      //  Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Product_info(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Delete_product(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void Delete_service(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }

    public static void signIn(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        Alerts.showDialog(dialog, "Please wait...");
        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                ResponceHandler.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                webResponse.onResponseFailed(t.getMessage());
            }
        });
    }
}


