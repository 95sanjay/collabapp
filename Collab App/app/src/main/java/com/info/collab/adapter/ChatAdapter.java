package com.info.collab.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.info.collab.R;
import com.info.collab.modalClass.ChatMD;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ImageViewHolder> {
    ArrayList<ChatMD> imageArrayList;
    OnItemClick onItemClick;

    public ChatAdapter(ArrayList<ChatMD> imageArrayList, OnItemClick onItemClick) {
        this.imageArrayList = imageArrayList;
        this.onItemClick = onItemClick ;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ImageViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_item_row, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int i) {
        ChatMD product = imageArrayList.get(i);

        imageViewHolder.content_tv.setText(product.getMessage());
        imageViewHolder.title_tv.setText(product.getBusiness_name());
        imageViewHolder.time_tv.setText(product.getCreated_at());

        Glide.with(imageViewHolder.logoImg.getContext())
                .load(product.getBusiness_logo())
                .error(R.drawable.test_logo)
                .into(imageViewHolder.logoImg);

        imageViewHolder.item_row_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onItemClick(product);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageArrayList.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder  {
        CircleImageView logoImg;
        TextView content_tv,time_tv,title_tv;
        RelativeLayout item_row_layout;

        public ImageViewHolder(View itemView) {
            super(itemView);
            time_tv = itemView.findViewById(R.id.time_tv);
            title_tv = itemView.findViewById(R.id.title_tv);
            content_tv = itemView.findViewById(R.id.content_tv);
            logoImg = itemView.findViewById(R.id.logoImg);
            item_row_layout = itemView.findViewById(R.id.item_row_layout);
        }
    }

    public interface OnItemClick {
        void onItemClick(ChatMD item);
    }
}
