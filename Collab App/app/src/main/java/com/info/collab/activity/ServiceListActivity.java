package com.info.collab.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.info.collab.R;
import com.info.collab.adapter.ServiceListAdapter;
import com.info.collab.modalClass.ServiceMD;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.BaseActivity;
import com.info.collab.utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class ServiceListActivity extends BaseActivity implements View.OnClickListener{

    RecyclerView feed_rv;
    private ArrayList<ServiceMD> servicelist;
    private ServiceListAdapter productAdapter;
    ImageView addBtn,backBtn;

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.service_list);
        setStatusBarGradiant(this);
        init();
    }

    void init(){
        feed_rv = findViewById(R.id.feed_rv);
        backBtn = findViewById(R.id.backBtn);
        addBtn = findViewById(R.id.addBtn);
        backBtn.setOnClickListener(this);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ServiceListActivity.this, AddServiceActivity.class);
                startActivityForResult(intent,333);
            }
        });
        serviceApi();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 333 && resultCode == 444) {
            serviceApi();
        }else {

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.backBtn:
                onBackPressed();
                break;
        }
    }

    private void serviceApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(getApplicationContext(), Constant.uni_id ));

        RequestBody requestBody = builder.build();
        RetrofitService.Service_list(new Dialog(ServiceListActivity.this), retrofitApiClient.Service_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson(response);
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void parseJson(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject resObj = null;
            resObj = new JSONObject(response.body().string());
            String message = resObj.optString("message");
            String status = resObj.optString("status");
            JSONArray jsonArray = null;
            if (status.equals("true")) {
                jsonArray = resObj.getJSONArray("service_list");
                JSONObject object = null;
                servicelist = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    object = jsonArray.getJSONObject(i);
                    ServiceMD product = new ServiceMD();
                    product.setId(object.getString("id"));
                    product.setCreated_by(object.getString("created_by"));
                    product.setEnglish_name(object.getString("english_name"));
                    product.setService_description(object.getString("service_description"));
                    product.setService_main_image(object.getString("service_main_image"));
                    product.setCategory(object.getString("category"));
                    product.setCreated_date_time(object.getString("created_date_time"));
                    JSONArray jsonArray1 = object.getJSONArray("created_by");
                    for (int j = 0 ; j < jsonArray1.length(); j++){
                        JSONObject object1 = jsonArray1.getJSONObject(j);
                        product.setBusiness_name(object1.getString("business_name"));
                        product.setBusiness_address(object1.getString("business_address"));
                        product.setCreated_by_id(object1.getString("uni_id"));
                    }
                    servicelist.add(product);
                }
                productAdapter = new ServiceListAdapter(servicelist, new ServiceListAdapter.OnServiceClick() {
                    @Override
                    public void onServiceDelete(ServiceMD item) {
                        deleteApi(item.getId());
                    }

                    @Override
                    public void onServiceEdit(ServiceMD item , String str) {
                        if (str.equalsIgnoreCase("next")){
                            Intent intent = new Intent(getApplicationContext(), ServiceInfo.class);
                            intent.putExtra("service",item);
                            startActivity(intent);
                        }else {
                            Intent i = new Intent(ServiceListActivity.this,UpdateServiceActivity.class);
                            i.putExtra("service", item);
                            startActivityForResult(i,333);
                        }
                    }
                });
                feed_rv.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                feed_rv.setItemAnimator(new DefaultItemAnimator());
                feed_rv.setAdapter(productAdapter);
                productAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {

        }
    }

    private void deleteApi(String id){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(getApplicationContext(), Constant.uni_id ))
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(getApplicationContext(), Constant.token_id ))
                .addFormDataPart("service_id",id);

        RequestBody requestBody = builder.build();
        RetrofitService.Delete_service(new Dialog(ServiceListActivity.this), retrofitApiClient.Delete_service(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson2(response);
            }

            @Override
            public void onResponseFailed(String error) {
            }
        });
    }

    private void parseJson2(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject jsonObject = new JSONObject(response.body().string());
            if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                Toast.makeText(this,jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                serviceApi();
            }
        } catch (Exception e) {

        }
    }
}