package com.info.collab.fragements;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.info.collab.R;
import com.info.collab.activity.AddProductActivity;
import com.info.collab.activity.CompanyProfileActivity;
import com.info.collab.activity.FeedPostActivity;
import com.info.collab.adapter.FeedAdapter;
import com.info.collab.modalClass.CollabMD;
import com.info.collab.modalClass.Feed;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class FeedFragment extends BaseFragment  {
    private static final int REQUEST_CODE = 11;
    public static final int RESULT_CODE = 12;
    public static final String EXTRA_KEY_TEST = "testKey";
    private BaseFragment openFragment;
    private View view;
    RecyclerView feed_rv;
    ArrayList<Feed> feedArrayList = new ArrayList<>();
    FeedAdapter feedAdapter;
    ImageView addBtn;
    int position;
    private String uni_id;
    private int a;

    public static FeedFragment newInstance(Bundle args) {
        FeedFragment fragment = new FeedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_feed, container, false);
        init();
        return view;
    }

    void  init(){
        uni_id = AppPreferences.getStringPreference(getContext(), Constant.uni_id );
        feed_rv = view.findViewById(R.id.feed_rv);
        addBtn = view.findViewById(R.id.addBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), FeedPostActivity.class);
                startActivityForResult(intent,11);
            }
        });
        feedApi();
        SwipeRefreshLayout pullToRefresh = view.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                feedApi();
                pullToRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 11 && resultCode == 12) {
            feedApi();
        }else {
        }
    }

    private void feedApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", uni_id)
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(getContext(), Constant.token_id ));

        RequestBody requestBody = builder.build();
        RetrofitService.Live_feed(new Dialog(getContext()), retrofitApiClient.Live_feed(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson(response);
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void parseJson(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject resObj = null;
            resObj = new JSONObject(response.body().string());
            String message = resObj.optString("message");
            String status = resObj.optString("status");
            JSONArray jsonArray = null;
            if (status.equals("true")) {
                jsonArray = resObj.getJSONArray("livefeed");
                JSONObject object = null;
                feedArrayList = new ArrayList<>();
                for (int i = 0 ; i < jsonArray.length() ; i++){
                    object = jsonArray.getJSONObject(i);
                    Feed product = new Feed();
                    product.setId(object.getString("id"));
                    product.setUser_id(object.getString("user_id"));
                    product.setUni_id(object.getString("uni_id"));
                    product.setImage(object.getString("image"));
                    product.setStory(object.getString("story"));
                    product.setBusiness_logo(object.getString("business_logo"));
                    product.setBusiness_cover_image(object.getString("business_cover_image"));
                    product.setBusiness_name(object.getString("business_name"));
                    product.setCreated_at(object.getString("created_at"));
                    product.setUpdated_at(object.getString("updated_at"));
                    JSONArray array = object.getJSONArray("comment");
                    product.setLike(object.getString("like_count"));
                    product.setIs_liked(object.getString("is_liked"));
                    product.setIs_follow(object.getString("is_follow"));
                    product.setPos(position);
                    product.setJsonArray(array);
                    feedArrayList.add(product);
                }
                feedAdapter = new FeedAdapter(getContext(), feedArrayList, new FeedAdapter.OnFeedClick() {
                    @Override
                    public void onLikeClick(Feed item,String like) {
                        if (like.equalsIgnoreCase("profile")){
                            AppPreferences.setStringPreference(appContext, Constant.collab_uni_id, item.getUni_id());
                            CollabMD collabMD = new CollabMD();
                            collabMD.setBusiness_name(item.getBusiness_name());
                            collabMD.setBusiness_logo(item.getBusiness_logo());
                            collabMD.setBusiness_cover_image(item.getBusiness_cover_image());

                            Intent intent = new Intent(getContext(), CompanyProfileActivity.class);
                            intent.putExtra("Collab", collabMD);
                            startActivity(intent);

                        }else {
                            SendLikeApi(item ,like);
                        }
                    }

                    @Override
                    public void onCommentClick(Feed item,String comment,int pos) {
                        position = pos;
                        SendCommentApi(item ,comment);
                    }

                    @Override
                    public void onDeleteClick(Feed item) {
                        deleteApi(item.getId());
                    }

                   public void onFollowClick(Feed item , boolean isFollow){
                       SendFollowApi(item,isFollow);
                       //Toast.makeText(appContext, item.getUser_id()+"\n"+isFollow, Toast.LENGTH_SHORT).show();
                   }

                });
                LinearLayoutManager layoutManager = new LinearLayoutManager(appContext,LinearLayoutManager.VERTICAL,false);
                feed_rv.setLayoutManager(layoutManager);
                feed_rv.setItemAnimator(new DefaultItemAnimator());
                feed_rv.setAdapter(feedAdapter);
                feedAdapter.notifyDataSetChanged();
                feed_rv.smoothScrollToPosition(position);
                feed_rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                            Log.d("-----","end");
                            Log.d("visiblePosition>>  ",""+ ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition());
                          //  feedApi();
                        }
                    }

                    @Override
                    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                    }
                });
            }else {
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void SendFollowApi(Feed item, boolean like) {
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", uni_id)
                .addFormDataPart("business_id", item.getId());

        RequestBody requestBody = builder.build();
        RetrofitService.Follow(new Dialog(getContext()), retrofitApiClient.Follow(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    if (status.equals("true")) {
                         Toast.makeText(appContext, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void SendLikeApi(Feed item, String like) {
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", uni_id)
                .addFormDataPart("post_id", item.getId() )
                .addFormDataPart("post_like", like )
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(getContext(), Constant.token_id ));

        RequestBody requestBody = builder.build();
        RetrofitService.Post_like(new Dialog(getContext()), retrofitApiClient.Post_like(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    if (status.equals("true")) {
                      //  Toast.makeText(appContext, message, Toast.LENGTH_SHORT).show();
                       // feedApi();
                    }else {
                        Toast.makeText(appContext, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void SendCommentApi(Feed item, String comment) {
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", uni_id)
                .addFormDataPart("post_id", item.getId() )
                .addFormDataPart("comment", comment );

        RequestBody requestBody = builder.build();
        RetrofitService.Post_comment(new Dialog(getContext()), retrofitApiClient.Post_comment(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    if (status.equals("true")) {
                        Toast.makeText(appContext, message, Toast.LENGTH_SHORT).show();
                        feedApi();
                    }else {
                        Toast.makeText(appContext, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void deleteApi(String id){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", uni_id)
                .addFormDataPart("post_id", id )
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(getContext(), Constant.token_id ));

        RequestBody requestBody = builder.build();
        RetrofitService.Delete_live_feed(new Dialog(getContext()), retrofitApiClient.Delete_live_feed(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson2(response);
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void parseJson2(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject resObj = null;
            resObj = new JSONObject(response.body().string());
            String message = resObj.optString("message");
            String status = resObj.optString("status");
            if (status.equals("true")) {
                Toast.makeText(appContext, message, Toast.LENGTH_SHORT).show();
                feedApi();
            }
        } catch (Exception e) {

        }
    }
}