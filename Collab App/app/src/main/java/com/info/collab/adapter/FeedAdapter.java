package com.info.collab.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.info.collab.R;
import com.info.collab.modalClass.CommentMd;
import com.info.collab.modalClass.Feed;
import com.info.collab.modalClass.Product;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.Constant;
import org.json.JSONException;
import java.util.ArrayList;
import android.os.Handler;

import de.hdodenhof.circleimageview.CircleImageView;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ImageViewHolder> {
    ArrayList<Feed> imageArrayList;
    Context context;
    OnFeedClick onFeedClick;
    int x;
    Handler handler ;

    public FeedAdapter(Context context,ArrayList<Feed> imageArrayList,OnFeedClick onFeedClick) {
        this.imageArrayList = imageArrayList;
        this.context = context;
        this.onFeedClick = onFeedClick;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ImageViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.feed_items, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int i) {
        Feed product = imageArrayList.get(i);
        imageViewHolder.text_p_type.setText(product.getStory());
        if (product.getStory().length() <= 100){
            imageViewHolder.text_p_type2.setVisibility(View.GONE);
        }else {
            imageViewHolder.text_p_type.setLines(2);
            imageViewHolder.text_p_type2.setVisibility(View.VISIBLE);
        }
        imageViewHolder.text_p_type2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageViewHolder.text_p_type.setMaxLines(50);
                imageViewHolder.text_p_type2.setVisibility(View.GONE);}
        });

        imageViewHolder.nameTV.setText(product.getBusiness_name());
        imageViewHolder.timeTV.setText(product.getCreated_at());
        Glide.with(imageViewHolder.IType.getContext()).load(product.getImage()).error(R.drawable.test_poster).into(imageViewHolder.IType);
        Glide.with(imageViewHolder.userImg.getContext()).load(product.getBusiness_logo()).error(R.drawable.test_logo).into(imageViewHolder.userImg);

        if (product.getUser_id().equalsIgnoreCase(AppPreferences.getStringPreference(context,Constant.uni_id))){
            imageViewHolder.delete_Img.setVisibility(View.VISIBLE);
            imageViewHolder.delete_Img.setOnClickListener(view -> {
                onFeedClick.onDeleteClick(product);
            });
        }

        if (product.getLike().equalsIgnoreCase("0")){
            imageViewHolder.conutTV.setVisibility(View.GONE);
        }else {
            imageViewHolder.conutTV.setText(product.getLike());
        }

        int n = Integer.parseInt(product.getLike());

        /*if (product.getIs_liked().equalsIgnoreCase("true")){
            imageViewHolder.like_btn.setChecked(true);
            imageViewHolder.like_btn.setButtonDrawable(R.drawable.heart);
        }*/

        imageViewHolder.like_btn.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b){
                onFeedClick.onLikeClick(product,"1");
                 x = n+1;
                imageViewHolder.conutTV.setText(""+x);
            }else {
                onFeedClick.onLikeClick(product,"0");
                int x1 = x-1;
                imageViewHolder.conutTV.setText(""+x1);
            }
        });

        if (product.getPos() == i  && product.getPos() != 0){
            imageViewHolder.comment_layout.setVisibility(View.VISIBLE);
        }

        imageViewHolder.customSwitch.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b){
                imageViewHolder.comment_layout.setVisibility(View.VISIBLE);
            }else {
                imageViewHolder.comment_layout.setVisibility(View.GONE);
            }
        });

            imageViewHolder.send_btn.setOnClickListener(view -> {
                imageViewHolder.send_btn.setVisibility(View.GONE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        imageViewHolder.send_btn.setVisibility(View.VISIBLE);
                    }
                },3000);
                onFeedClick.onCommentClick(product, imageViewHolder.comment_edt.getText().toString().trim(),i);
            });

        try {
        ArrayList<CommentMd> arrayList = new ArrayList<CommentMd>();
        for (int j = 0; j < product.getJsonArray().length() ; j++){
                CommentMd commentMd = new CommentMd();
                commentMd.setId(product.getJsonArray().getJSONObject(j).getString("id"));
                commentMd.setUser_id(product.getJsonArray().getJSONObject(j).getString("user_id"));
                commentMd.setPost_id(product.getJsonArray().getJSONObject(j).getString("post_id"));
                commentMd.setComment(product.getJsonArray().getJSONObject(j).getString("comment"));
                commentMd.setBusiness_name(product.getJsonArray().getJSONObject(j).getString("business_name"));
                commentMd.setBusiness_logo(product.getJsonArray().getJSONObject(j).getString("business_logo"));
                commentMd.setTime(product.getJsonArray().getJSONObject(j).getString("created_at"));
                arrayList.add(commentMd);
        }
            CommentAdp commentAdp = new CommentAdp(arrayList);
            imageViewHolder.comment_rv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            imageViewHolder.comment_rv.setItemAnimator(new DefaultItemAnimator());
            imageViewHolder.comment_rv.setAdapter(commentAdp);

            imageViewHolder.comment_rv.setOnTouchListener(new View.OnTouchListener() {
                // Setting on Touch Listener for handling the touch inside ScrollView
                @SuppressLint("ClickableViewAccessibility")
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // Disallow the touch request for parent scroll on touch of child view
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

        imageViewHolder.IType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog(product.getImage());
            }
        });

        imageViewHolder.profile_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onFeedClick.onLikeClick(product,"profile");
            }
        });

        imageViewHolder.follow_btn.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b){
                onFeedClick.onFollowClick(product,b);
            }else {
                onFeedClick.onFollowClick(product,b);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageArrayList.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder  {
        androidx.appcompat.widget.AppCompatImageView IType,delete_Img;
        TextView text_p_type,nameTV,timeTV,conutTV,text_p_type2;
        CircleImageView userImg;
        LinearLayout comment_layout,send_btn;
        EditText comment_edt;
        RecyclerView comment_rv;
        SwitchCompat customSwitch,like_btn,follow_btn;
        RelativeLayout profile_layout;

        public ImageViewHolder(View itemView) {
            super(itemView);
            IType = itemView.findViewById(R.id.IType);
            text_p_type = itemView.findViewById(R.id.text_p_type);
            userImg = itemView.findViewById(R.id.userImg);
            nameTV = itemView.findViewById(R.id.nameTV);
            timeTV = itemView.findViewById(R.id.timeTV);
            conutTV = itemView.findViewById(R.id.conutTV);
            like_btn = itemView.findViewById(R.id.like_btn);
            delete_Img = itemView.findViewById(R.id.delete_Img);
            comment_layout = itemView.findViewById(R.id.comment_layout);
            send_btn = itemView.findViewById(R.id.send_btn);
            comment_edt = itemView.findViewById(R.id.comment_edt);
            comment_rv = itemView.findViewById(R.id.comment_rv);
            customSwitch = itemView.findViewById(R.id.customSwitch);
            follow_btn = itemView.findViewById(R.id.follow_btn);
            text_p_type2 = itemView.findViewById(R.id.text_p_type2);
            profile_layout = itemView.findViewById(R.id.profile_layout);
        }
    }

    public interface OnFeedClick {
        void onLikeClick(Feed item,String s);
        void onCommentClick(Feed item, String comment,int pos);
        void onDeleteClick(Feed item);
        void onFollowClick(Feed item,boolean msg);
    }

    public void dialog(String url){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.image_dialog);
        ImageView image = dialog.findViewById(R.id.image);
        ImageView cancel = dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Glide.with(image.getContext()).load(url).error(R.drawable.test_poster).into(image);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.show();

    }
}
