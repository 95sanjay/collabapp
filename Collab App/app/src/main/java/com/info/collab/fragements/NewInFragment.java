package com.info.collab.fragements;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.info.collab.R;
import com.info.collab.activity.ChatActivity;
import com.info.collab.activity.ProductInfo;
import com.info.collab.adapter.ChatAdapter;
import com.info.collab.adapter.NewInAdapter;
import com.info.collab.adapter.ProductAdapter;
import com.info.collab.modalClass.Feed;
import com.info.collab.modalClass.Product;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class NewInFragment extends BaseFragment {
    RecyclerView chat_rv;
    private View view;
    private ArrayList<Feed> chatArrayList = new ArrayList<>();
    private ChatAdapter feedAdapter;
    private ArrayList<Product> productslist;
    private SearchView edt_search;
    private NewInAdapter productAdapter;

    public static NewInFragment newInstance(Bundle args) {
        NewInFragment fragment = new NewInFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.new_in_fragment, container, false);
        init();
        return view;
    }

    void init(){
        chat_rv = view.findViewById(R.id.chat_rv);
        edt_search = view.findViewById(R.id.edt_search);
        ImageView back_btn = view.findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        TextView title = view.findViewById(R.id.title);
        title.setText("New In Products");
        productApi();
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        edt_search.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        edt_search.setMaxWidth(Integer.MAX_VALUE);
        edt_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                productAdapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                productAdapter.getFilter().filter(s);
                return false;
            }
        });
    }


    private void productApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(getContext(), Constant.uni_id ))
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(getContext(), Constant.token_id ));
        RequestBody requestBody = builder.build();
        RetrofitService.NewInProduct_list(new Dialog(getContext()), retrofitApiClient.NewInProduct_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("product_list");
                        JSONObject object = null;
                        productslist = new ArrayList<>();
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            object = jsonArray.getJSONObject(i);
                            Product product = new Product();
                            product.setProduct_identifier(object.getString("product_identifier"));
                            product.setBID(object.getString("BID"));
                            product.setHs_code(object.getString("hs_code"));
                            product.setCreated_by(object.getString("created_by"));
                            product.setSeller_type(object.getString("seller_type"));
                            product.setArabic_name(object.getString("arabic_name"));
                            product.setEnglish_name(object.getString("english_name"));
                            product.setProduct_category_id(object.getString("product_category_id"));
                            product.setProduct_sub_category_id(object.getString("product_sub_category_id"));
                            product.setProduct_description(object.getString("product_description"));
                            product.setProduct_main_image(object.getString("product_main_image"));
                            product.setProduct_creation_date(object.getString("product_creation_date"));
                            productslist.add(product);
                        }
                         productAdapter = new NewInAdapter(productslist, item -> {
                            Intent intent = new Intent(getActivity(), ProductInfo.class);
                            intent.putExtra("product_id",item.getProduct_identifier());
                            startActivity(intent);
                        });
                        chat_rv.setLayoutManager(new GridLayoutManager(getContext(), 2));
                        chat_rv.setItemAnimator(new DefaultItemAnimator());
                        chat_rv.setAdapter(productAdapter);
                        productAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseFailed(String error) {
            }
        });
    }
}