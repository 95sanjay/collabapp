package com.info.collab.fragements;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.info.collab.R;
import com.info.collab.activity.ChatActivity;
import com.info.collab.adapter.ChatAdapter;
import com.info.collab.modalClass.ChatMD;
import com.info.collab.modalClass.Feed;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class CollabFragment extends BaseFragment {
    RecyclerView chat_rv;
    private View view;
    private ArrayList<ChatMD> chatArrayList = new ArrayList<>();
    private ChatAdapter feedAdapter;
    Handler handler ;
    private Runnable runnableCode;

    public static CollabFragment newInstance(Bundle args) {
        CollabFragment fragment = new CollabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_collab, container, false);
        init();
        return view;
    }

    void init(){
        Userchat_listApi();
        chat_rv = view.findViewById(R.id.chat_rv);
        TextView title = view.findViewById(R.id.title);
        title.setText("Collab");
        SwipeRefreshLayout pullToRefresh = view.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Userchat_listApi();
                pullToRefresh.setRefreshing(false);
            }
        });

      /*  handler = new Handler();
        runnableCode = new Runnable() {
            @Override
            public void run() {
                Userchat_listApi();
                handler.postDelayed(this, 1000);
            }
        };
        handler.post(runnableCode);*/
    }

   /* @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(runnableCode);
    }

     @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnableCode);
    }*/

    private void Userchat_listApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(getContext(), Constant.uni_id ));

        RequestBody requestBody = builder.build();
        RetrofitService.Userchat_list(new Dialog(getContext()), retrofitApiClient.Userchat_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                parseJson(response);
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void parseJson(retrofit2.Response<ResponseBody> response) {
        try {
            JSONObject resObj = null;
            resObj = new JSONObject(response.body().string());
            String message = resObj.optString("message");
            String status = resObj.optString("status");
            JSONArray jsonArray = null;
            if (status.equals("true")) {
                jsonArray = resObj.getJSONArray("data");
                JSONObject object = null;
                chatArrayList = new ArrayList<>();
                for (int i = 0 ; i < jsonArray.length() ; i++){
                    object = jsonArray.getJSONObject(i);
                    ChatMD product = new ChatMD();
                    product.setBusiness_logo(object.getString("business_logo"));
                    product.setBusiness_name(object.getString("business_name"));
                    product.setUser1_id(object.getString("uni_id"));
                    product.setMessage(object.getString("last_message"));
                    product.setCreated_at(object.getString("created_at"));
                    chatArrayList.add(product);
                }

                feedAdapter = new ChatAdapter(chatArrayList, new ChatAdapter.OnItemClick() {
                    @Override
                    public void onItemClick(ChatMD item) {
                        Intent i = new Intent(getActivity(), ChatActivity.class);
                        i.putExtra("user2_id", item.getUser1_id());
                        i.putExtra("imageurl",item.getBusiness_logo());
                        i.putExtra("business_name",item.getBusiness_name());
                        startActivity(i);
                    }
                });

                chat_rv.setLayoutManager(new LinearLayoutManager(appContext, LinearLayoutManager.VERTICAL, false));
                chat_rv.setItemAnimator(new DefaultItemAnimator());
                chat_rv.setAdapter(feedAdapter);
                feedAdapter.notifyDataSetChanged();
            }else {
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}