package com.info.collab.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.info.collab.R;
import com.info.collab.adapter.CategoryAdapter;
import com.info.collab.modalClass.CategoryMD;
import com.info.collab.modalClass.ServiceMD;
import com.info.collab.modalClass.SubCategoryMD;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.Alerts;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.BaseActivity;
import com.info.collab.utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class UpdateServiceActivity extends BaseActivity implements View.OnClickListener {

    ImageView add_img,backBtn;
    EditText name_edt,discription_edt;
    public final int REQUEST_CODE_PICKER = 125;
    ArrayList<Image> images = new ArrayList<>();
    byte[] imageBytes;
    String timeStamp="";
    String extension="";
    TextView btn_add;
    private ServiceMD product;
    Spinner spin_categoury;
    private String categoury_selected;
    private ArrayList<CategoryMD> categoryMDArrayList;
    private ArrayList<CategoryMD> categoryMDArrayList2 = new ArrayList<>();
    private ArrayList<SubCategoryMD> subCategoryMDArrayList;
    private ArrayList<SubCategoryMD> subCategoryMDArrayList2 = new ArrayList<>();
    private String main_activity,sub_activity;

    protected void setStatusBarGradiant(Activity activity) {
        Window window = activity.getWindow();
        @SuppressLint("UseCompatLoadingForDrawables") Drawable background = activity.getResources().getDrawable(R.drawable.ic_rectangle_top);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        setContentView(R.layout.add_service);
        init();
    }

    void  init(){
        add_img = findViewById(R.id.add_img);
        backBtn = findViewById(R.id.backBtn);
        btn_add = findViewById(R.id.btn_add);
        name_edt = findViewById(R.id.name_edt);
        spin_categoury = findViewById(R.id.spin_categoury);
        discription_edt = findViewById(R.id.discription_edt);
        TextView title = findViewById(R.id.title);
        title.setText("Update Service");

        add_img.setOnClickListener(this);
        backBtn.setOnClickListener(this);
        btn_add.setOnClickListener(this);

        Intent i = getIntent();
        product = (ServiceMD) i.getSerializableExtra("service");
        if (i != null){
            btn_add.setText("Update Service");
            name_edt.setText(product.getEnglish_name());
            discription_edt.setText(product.getService_description());
            Glide.with(add_img.getContext()).load(product.getService_main_image()).into(add_img);
        }
        categouryApi();
    }


    private void categouryApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", "1");
        RequestBody requestBody = builder.build();
        RetrofitService.Company_category_list(new Dialog(UpdateServiceActivity.this), retrofitApiClient.Company_category_list(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        jsonArray = resObj.getJSONArray("data");
                        JSONObject object = null;
                        categoryMDArrayList = new ArrayList<>();
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            object = jsonArray.getJSONObject(i);
                            CategoryMD categoryMD = new CategoryMD();
                            categoryMD.setId(object.getString("id"));
                            categoryMD.setName(object.getString("name"));
                            categoryMDArrayList.add(categoryMD);
                        }
                        CategoryMD categoryMD = new CategoryMD();
                        categoryMD.setId("0");
                        categoryMD.setName("Select");
                        categoryMDArrayList2.add(categoryMD);
                        categoryMDArrayList2.addAll(categoryMDArrayList);
                        CategoryAdapter categoryAdapter = new CategoryAdapter(getApplicationContext(),categoryMDArrayList2);
                        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spin_categoury.setAdapter(categoryAdapter);
                        spin_categoury.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                main_activity = categoryMDArrayList2.get(i).getId();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                }catch (Exception e){}
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void UpdateApi(){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", AppPreferences.getStringPreference(getApplicationContext(), Constant.uni_id ))
                .addFormDataPart("tokenid", AppPreferences.getStringPreference(getApplicationContext(), Constant.token_id ))
                .addFormDataPart("english_name", name_edt.getText().toString().trim())
                .addFormDataPart("category", main_activity)
                .addFormDataPart("service_id",product.getId() )
                .addFormDataPart("description", discription_edt.getText().toString().trim());

        if (images.size() != 0) {
            if (images.get(0).getPath() != null) {
                File featured_image = new File(images.get(0).getPath());
                if (featured_image.exists()) {
                    builder.addFormDataPart("service_mainimg", featured_image.getName(), RequestBody.create(MultipartBody.FORM, featured_image));
                }
            }
        }

        RequestBody requestBody = builder.build();
        RetrofitService.Update_service(new Dialog(UpdateServiceActivity.this), retrofitApiClient.Edit_service(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(Response<?> result) {
                Response<ResponseBody> response = (Response<ResponseBody>) result;
                parseJson(response);
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }

    private void parseJson(Response<ResponseBody> response) {
        try {
            JSONObject jsonObject = new JSONObject(response.body().string());
            if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                setResult(444, intent);
                finish();
            }
        } catch (Exception e) {
            Log.e("sanjUPS",""+e.getMessage());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_add:

                if (isInputValid()){
                    UpdateApi();
                }
                break;

            case R.id.add_img:
                selectimg();
                break;

            case R.id.backBtn:
                onBackPressed();
                break;
        }
    }


    void selectimg() {
        ImagePicker.create(this)
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .theme(R.style.ImagePickerTheme)
                .single() // max images can be selected (99 by default)
                .showCamera(false) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image ("Camera" folder by default)
                .origin(images) // original selected images, used in multi mode
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }


    @Override
    public void onActivityResult(int requestCode, final int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICKER && resultCode == Activity.RESULT_OK && data != null) {
            images.clear();
            images.addAll(ImagePicker.getImages(data));
            if (images.size() > 0) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeFile(images.get(0).getPath());
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                imageBytes = baos.toByteArray();
                String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                Long tsLong = System.currentTimeMillis() / 1000;
                timeStamp = tsLong.toString();
                String filenameArray[] = images.get(0).getName().split("\\.");
                extension = filenameArray[filenameArray.length - 1];

              //  Log.d("image","}}}}}}}}}}===  "+imageBytes+"\n"+timeStamp+"\n"+extension);
                add_img.setImageBitmap(bitmap);
            }
        }
    }

    private boolean isInputValid() {
        if (name_edt.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter service name.");
            return false;
        } else if (main_activity.equalsIgnoreCase("Select")) {
            Alerts.showToast(this, "Please enter category .");
            return false;
        } else if (discription_edt.getText().toString().trim().length() == 0) {
            Alerts.showToast(this, "Please enter discription.");
            return false;
        }/*else if (images.size() == 0) {
            Alerts.showToast(this, "Please select Image.");
            return false;
        }*/
        return true;
    }
}

//https://stackoverflow.com/questions/52553210/retrofit-2-multipart-image-upload-with-data