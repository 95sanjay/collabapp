package com.info.collab.fragements;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.info.collab.R;
import com.info.collab.activity.AddProductActivity;
import com.info.collab.activity.Signup2Activity;
import com.info.collab.activity.UpdateProfileActivity;
import com.info.collab.modalClass.ProductCategory;
import com.info.collab.retrofits_provider.RetrofitService;
import com.info.collab.retrofits_provider.WebResponse;
import com.info.collab.utils.AppPreferences;
import com.info.collab.utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class BusinessInfoFragment extends BaseFragment implements View.OnClickListener {

    private View view;
    TextView business_nameTV,mobileTV,bussiness_bio_tv;
    LinearLayout editProfile;

    public static BusinessInfoFragment newInstance(Bundle args) {
        BusinessInfoFragment fragment = new BusinessInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.business_info, container, false);
        init();
        return view;
    }

    void  init(){
        business_nameTV = view.findViewById(R.id.business_nameTV);
        mobileTV = view.findViewById(R.id.mobileTV);
        bussiness_bio_tv = view.findViewById(R.id.bussiness_bio_tv);
        editProfile = view.findViewById(R.id.editProfile);
        editProfile.setOnClickListener(this);

        business_nameTV.setText(AppPreferences.getStringPreference(getContext(), Constant.business_name));
        mobileTV.setText(AppPreferences.getStringPreference(getContext(),Constant.business_main_phone_number));
        bussiness_bio_tv.setText(AppPreferences.getStringPreference(getContext(),Constant.business_bio));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.editProfile:
                Intent intent = new Intent(getContext(), UpdateProfileActivity.class);
                startActivity(intent);
                break;
        }
    }

    public void editDialog(String message){
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.edit_profile);
        EditText edt_name = dialog.findViewById(R.id.edt_name);
        EditText edt_phone = dialog.findViewById(R.id.edt_phone);
        EditText edt_bio = dialog.findViewById(R.id.edt_bio);
        edt_name.setText(AppPreferences.getStringPreference(getContext(),Constant.business_name));
        edt_phone.setText(AppPreferences.getStringPreference(getContext(),Constant.business_main_phone_number));
        edt_bio.setText(AppPreferences.getStringPreference(getContext(),Constant.business_bio));
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        Button declineButton = (Button) dialog.findViewById(R.id.btnDialogCancel);
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button videoButton = (Button) dialog.findViewById(R.id.btnDialogOk);
        videoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                UpdateProfile(edt_name.getText().toString().trim(), edt_phone.getText().toString().trim(), edt_bio.getText().toString().trim());
            }
        });
    }


    private void UpdateProfile(String name , String phone , String bio){
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("name",name )
                .addFormDataPart("phone_no", phone)
                .addFormDataPart("user_id", AppPreferences.getStringPreference(getContext(),Constant.uni_id))
                .addFormDataPart("bio",bio);
               /* .addFormDataPart("username", AppPreferences.getStringPreference(getContext(),Constant.business_username))
                .addFormDataPart("ownername", AppPreferences.getStringPreference(getContext(),Constant.business_owner))
                .addFormDataPart("email_id", AppPreferences.getStringPreference(getContext(),Constant.business_email))
                .addFormDataPart("address", AppPreferences.getStringPreference(getContext(),Constant.business_address))
                .addFormDataPart("password", AppPreferences.getStringPreference(getContext(),Constant.business_password))
                .addFormDataPart("doc_type", AppPreferences.getStringPreference(getContext(),Constant.doc_type))
                .addFormDataPart("country", AppPreferences.getStringPreference(getContext(),Constant.business_country))
                .addFormDataPart("country_code", AppPreferences.getStringPreference(getContext(),Constant.business_country_code))
                .addFormDataPart("city", AppPreferences.getStringPreference(getContext(),Constant.business_city))
                .addFormDataPart("main_activity", AppPreferences.getStringPreference(getContext(),Constant.business_main_activity_id))
                .addFormDataPart("sub_activity", AppPreferences.getStringPreference(getContext(),Constant.business_sub_activity_id))
                .addFormDataPart("creation_date", AppPreferences.getStringPreference(getContext(),Constant.business_creation_date))
                .addFormDataPart("expiry_date", AppPreferences.getStringPreference(getContext(),Constant.business_exp_date))
                .addFormDataPart("device_id", AppPreferences.getStringPreference(getContext(),Constant.device_id));*/
               /* builder.addFormDataPart("cover_img", null);
                builder.addFormDataPart("logo", null);
                builder.addFormDataPart("shelf_doc", null);
                builder.addFormDataPart("auth_doc[]", null);*/

        RequestBody requestBody = builder.build();
        RetrofitService.Edit_user(new Dialog(getContext()), retrofitApiClient.Edit_user(requestBody), new WebResponse() {
            @Override
            public void onResponseSuccess(retrofit2.Response<?> result) {
                retrofit2.Response<ResponseBody> response = (retrofit2.Response<ResponseBody>) result;
                try {
                    JSONObject resObj = null;
                    resObj = new JSONObject(response.body().string());
                    String message = resObj.optString("message");
                    String status = resObj.optString("status");
                    JSONArray jsonArray = null;
                    if (status.equals("true")) {
                        Toast.makeText(appContext, message, Toast.LENGTH_SHORT).show();
                        jsonArray = resObj.getJSONArray("data");
                        JSONObject object = null;
                        for (int i = 0; i < jsonArray.length(); i++) {
                            object = jsonArray.getJSONObject(i);
                            AppPreferences.setStringPreference(appContext, Constant.business_name, object.getString("business_name"));
                            AppPreferences.setStringPreference(appContext, Constant.business_main_phone_number, object.getString("business_main_phone_number"));
                            AppPreferences.setStringPreference(appContext, Constant.business_bio, object.getString("business_bio"));

                            business_nameTV.setText(AppPreferences.getStringPreference(getContext(), Constant.business_name));
                            mobileTV.setText(AppPreferences.getStringPreference(getContext(),Constant.business_main_phone_number));
                            bussiness_bio_tv.setText(AppPreferences.getStringPreference(getContext(),Constant.business_bio));
                        }
                    }
                }catch (Exception e){

                }
            }

            @Override
            public void onResponseFailed(String error) {

            }
        });
    }
}